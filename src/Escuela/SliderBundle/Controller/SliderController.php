<?php

namespace Escuela\SliderBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Escuela\SliderBundle\Entity\Slider;
use Escuela\SliderBundle\Form\SliderType;
use Escuela\SliderBundle\Entity\TextSlider;
use Escuela\SliderBundle\Form\TextSliderType;
use Symfony\Component\HttpFoundation\JsonResponse;




/**
 * Slider controller.
 *
 */
class SliderController extends Controller
{

    /**
     * Lists all Slider entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SliderBundle:Slider')->findAll();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Slider");
        //End Breadcrumbs

        return $this->render('SliderBundle:Slider:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Slider entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Slider();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $directorioBase = $this->container->getParameter('uploads.slider.absoluto');

        $entity->subirFoto($directorioBase);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('slider_show', array('id' => $entity->getId())));
        }elseif(file_exists($directorioBase.$entity->getImagen())){
            unlink($directorioBase.$entity->getImagen());
        }

        return $this->render('SliderBundle:Slider:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Slider entity.
    *
    * @param Slider $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Slider $entity)
    {
        $form = $this->createForm(new SliderType(), $entity, array(
            'action' => $this->generateUrl('slider_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Slider entity.
     *
     */
    public function newAction()
    {
        $entity = new Slider();
        $form   = $this->createCreateForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Slider", $this->get("router")->generate("slider"));
        $breadcrumbs->addItem("Nuevo");
        //End Breadcrumbs

        return $this->render('SliderBundle:Slider:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Slider entity.
     *
     * @ParamConverter ("slider", class="SliderBundle:Slider")
     */
    public function showAction(Slider $slider)
    {
        $em = $this->getDoctrine()->getManager();

        $textSliders = $em->getRepository('SliderBundle:Slider')->findTextSlider($slider->getId());

        $deleteForm = $this->createDeleteForm($slider->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Slider", $this->get("router")->generate("slider"));
        $breadcrumbs->addItem("Ver");
        //End Breadcrumbs

        return $this->render('SliderBundle:Slider:show.html.twig', array(
            'entity'      => $slider,
            'textSlider'  => $textSliders,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Slider entity.
     *
     * @ParamConverter ("slider", class="SliderBundle:Slider")
     */
    public function editAction(Slider $slider)
    {
        if (!$slider) {
            throw $this->createNotFoundException('Unable to find Slider entity.');
        }
        $em = $this->getDoctrine()->getManager();
        $textSliders = $em->getRepository('SliderBundle:Slider')->findTextSlider($slider->getId());
        $textos = $em->getRepository('SliderBundle:TextSliderTranslation')->findAllTranslations($textSliders);

        $editForm = $this->createEditForm($slider);
        $editTextForm = $this->createEditTextForm(new TextSlider());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Slider", $this->get("router")->generate("slider"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('SliderBundle:Slider:edit.html.twig', array(
            'entity'      => $slider,
            'textos'  => $textos,
            'edit_text_form' => $editTextForm->createView(),
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Slider entity.
    *
    * @param Slider $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Slider $entity)
    {
        $form = $this->createForm(new SliderType(), $entity, array(
            'action' => $this->generateUrl('slider_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Slider entity.
     * @ParamConverter ("slider", class="SliderBundle:Slider")
     */
    public function updateAction(Request $request, Slider $entity)
    {
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Slider entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $directorioBase = $this->container->getParameter('uploads.slider.absoluto');

        $rutaFotoOriginal = $editForm->getData()->getImagen();
        if (null == $entity->getFoto()) {
            // La foto original no se modifica, recuperar su ruta
            $entity->setImagen($rutaFotoOriginal);
        }else {
            // La foto de la oferta se ha modificado
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
            $entity->subirFoto($directorioBase);
        }
        if ($editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('slider_show', array('id' => $entity->getId())));
        }
        $em = $this->getDoctrine()->getManager();
        $editTextForm = $this->createEditTextForm(new TextSlider());

        $textSliders = $em->getRepository('SliderBundle:Slider')->findTextSlider($entity->getId());
        $textos = $em->getRepository('SliderBundle:TextSliderTranslation')->findAllTranslations($textSliders);

        return $this->render('SliderBundle:Slider:edit.html.twig', array(
            'textos' => $textos,
            'entity'      => $entity,
            'edit_text_form' => $editTextForm->createView(),
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Slider entity.
     * @ParamConverter ("slider", class="SliderBundle:Slider")
     */
    public function deleteAction(Request $request, Slider $entity)
    {
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Slider entity.');
        }
        $form = $this->createDeleteForm($entity->getId());
        $form->handleRequest($request);
        $directorioBase = $this->container->getParameter('uploads.slider.absoluto');

        if ($form->isValid()) {
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }

            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('slider'));
    }

    /**
     * Creates a form to delete a Slider entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('slider_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Visualiza el formulario de eliminación
     *
     * @ParamConverter ("slider", class="SliderBundle:Slider")
     */
    public function removeAction(Slider $entity){
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Slider entity.');
        }

        $deleteForm = $this->createDeleteForm($entity->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Slider", $this->get("router")->generate("slider"));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('SliderBundle:Slider:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    private function createEditTextForm(TextSlider $entity)
    {
        $form = $this->createForm(new TextSliderType(), $entity, array(
            'action' => $this->generateUrl('slider_text_create_update'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Añadir'));

        return $form;
    }

    /**
     * @param Request $request
     * @return bool|\Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function updateCreateTextSliderAction(Request $request){
        $estado = false;
        $parameters = $request->get('escuela_sliderbundle_textslider');
        $em = $this->getDoctrine()->getManager();

        if(!$parameters['id']){ //Es nuevo
            $entity = new TextSlider();
            $form =  $this->createEditTextForm($entity);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em->persist($entity);
                $em->flush();
                $estado = true;
            }
        }else{ // Editas
            $entity = $em->getRepository('SliderBundle:TextSlider')->find($parameters['id']);
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TextSlider entity.');
            }
            $editForm =  $this->createEditTextForm($entity);
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $estado = true;
            }
        }

        $serializer = $this->get('jms_serializer');

        $data['state'] = $estado;
        if($estado){
            $data['fila'] = $serializer->serialize($entity, 'json');
        }

        $response = new JsonResponse();
        $response->headers->set('Content-Type', 'application/json');
        $response->setData($data);

        return $response;

    }

    /**
     * @ParamConverter ("textSlider", class="SliderBundle:TextSlider")
    */
    public function removeTextAction(TextSlider $textSlider){
        $deleteForm = $this->deleteTextForm($textSlider->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("el-campo"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Slider", $this->get("router")->generate("slider"));
        $breadcrumbs->addItem("Borrar Texto");
        //End Breadcrumbs

        return $this->render('SliderBundle:Slider:removeText.html.twig', array(
            'textSlider' => $textSlider,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteTextForm($id){
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('slider_delete_text', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Deletes a Slider entity.
     * @ParamConverter ("textSlider", class="SliderBundle:TextSlider")
     */
    public function deleteTextAction(Request $request, TextSlider $entity)
    {
        $form = $this->deleteTextForm($entity->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TextSlider entity.');
            }

            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('slider_edit', array('id' => $entity->getSlider()->getId())));
    }

    /**
     * @param Request $request
     * @param $id Texto del slider
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function ajaxGetTextAction(Request $request, $id){
//        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('SliderBundle:TextSlider')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TextSlider entity.');
            }

            $repository = $em->getRepository('SliderBundle:TextSliderTranslation');
            $translations = $repository->findTranslations($entity);

            $serializer = $this->get('jms_serializer');

            $response = new JsonResponse();
            $response->setData(array(
                'espanol' =>  $serializer->serialize($entity, 'json'),
                'translations' => $translations
            ));
            return $response;
      //  } else {
      //      throw $this->createNotFoundException('Solo es accesible via Ajax.');
      //  }
    }
}
