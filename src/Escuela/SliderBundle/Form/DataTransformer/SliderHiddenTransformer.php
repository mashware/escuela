<?php
/**
 * Created by PhpStorm.
 * User: Alberto
 * Date: 4/05/14
 * Time: 20:15
 */

namespace Escuela\SliderBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Escuela\SliderBundle\Entity\Slider;

class SliderHiddenTransformer implements DataTransformerInterface {
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }
    /**
     * Transforms an object (slider) to a string (number).
     *
     * @param  Slider|null $slider
     * @return string
     */
    public function transform($slider)
    {
        if (null === $slider) {
            return "";
        }

        return $slider->getId();
    }
    /**
     * Transforms a string (number) to an object (slider).
     *
     * @param  string $number
     *
     * @return Slider|null
     *
     * @throws TransformationFailedException if object (slider) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }

        $slider = $this->om
            ->getRepository('SliderBundle:Slider')
            ->find($number)
        ;

        if (null === $slider) {
            throw new TransformationFailedException(sprintf(
                'An slider with number "%s" does not exist!',
                $number
            ));
        }

        return $slider;
    }
} 