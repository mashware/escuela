<?php

namespace Escuela\SliderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TextSliderType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text')
            ->add('topCss')
            ->add('leftCss')
            ->add('widthCss')
            ->add('slider', 'slider_hidden')
            ->add('text_ingles', 'translated_field', array(
                'field'          => 'text',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\SliderBundle\Entity\TextSliderTranslation',
                'texto_label' => 'Text'
            ))
            ->add('topCss_ingles', 'translated_field', array(
                'field'          => 'topCss',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\SliderBundle\Entity\TextSliderTranslation',
                'texto_label' => 'Posición Top',
                'widget' => 'integer'
            ))
            ->add('leftCss_ingles', 'translated_field', array(
                'field'          => 'leftCss',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\SliderBundle\Entity\TextSliderTranslation',
                'texto_label' => 'Posición Left',
                'widget' => 'integer'
            ))
            ->add('widthCss_ingles', 'translated_field', array(
                'field'          => 'widthCss',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\SliderBundle\Entity\TextSliderTranslation',
                'texto_label' => 'Ancho',
                'widget' => 'integer'
            ))
        ;
        $builder->add('id', 'hidden', array(
            'mapped' => false,
            'required' => false
        ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\SliderBundle\Entity\TextSlider'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_sliderbundle_textslider';
    }
}
