<?php
    namespace Escuela\SliderBundle\DataFixtures\ORM;

    use Doctrine\Common\DataFixtures\AbstractFixture;
    use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
    use Doctrine\Common\Persistence\ObjectManager;
    use Escuela\SliderBundle\Entity\TextSlider;

    class TextFix extends AbstractFixture implements OrderedFixtureInterface
    {
        public function getOrder(){
            return 3;
        }

        public function load(ObjectManager $manager)
        {
            //obtengo todas los sliders
            $sliders = $manager->getRepository('SliderBundle:Slider')->findAll();

            foreach ($sliders as $slider) {
                $total = rand(0,2);
                for($i=0; $i<=$total; $i++){
                    $entidad = new TextSlider();

                    $entidad->setText('En un lugar de la Mancha, de cuyo nombre no quiero acordarme...');
                    $entidad->setSlider($slider);
                    $entidad->setLeftCss(rand(5, 200));
                    $entidad->setTopCss(rand(5, 100));
                    $entidad->setWidthCss(rand(100,400));

                    $manager->persist($entidad);
                }
            }
            $manager->flush();
        }
    }