<?php
    namespace Escuela\SliderBundle\DataFixtures\ORM;

    use Doctrine\Common\DataFixtures\AbstractFixture;
    use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
    use Doctrine\Common\Persistence\ObjectManager;
    use Escuela\SliderBundle\Entity\Slider;

    class SliderFix extends AbstractFixture implements OrderedFixtureInterface
    {
        public function getOrder(){
            return 2;
        }

        public function load(ObjectManager $manager)
        {
            $imagen = array('sky', 'vine', 'lava', 'gray', 'industrial', 'social');
            //obtengo todas las seecciones
            $secciones = $manager->getRepository('SliderBundle:SeccionSlider')->findAll();

            foreach ($secciones as $seccion) {
                for($i=0; $i<=5; $i++){
                    $entidad = new Slider();

                    $entidad->setImagen('holder.js/970x377/'.$imagen[rand(0,5)]);
                    $entidad->setPosition($i);
                    $entidad->setSeccion($seccion);
                    $entidad->setState(rand(0,1));
                    if(rand(0,1)){
                        $entidad->setUrl('www.google.es');
                    }

                    $manager->persist($entidad);
                }
            }
            $manager->flush();
        }
    }