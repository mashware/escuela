<?php
    namespace Escuela\SliderBundle\DataFixtures\ORM;

    use Doctrine\Common\DataFixtures\AbstractFixture;
    use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
    use Doctrine\Common\Persistence\ObjectManager;
    use Escuela\SliderBundle\Entity\SeccionSlider;

    class Seccion extends AbstractFixture implements OrderedFixtureInterface
    {
        public function getOrder(){
            return 1;
        }
        public function load(ObjectManager $manager)
        {
            $secciones = array('home');

            foreach ($secciones as $seccion) {
                $entidad = new SeccionSlider();

                $entidad->setName($seccion);

                $manager->persist($entidad);
            }

            $manager->flush();
        }
    }