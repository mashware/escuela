<?php

namespace Escuela\SliderBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Gedmo\Tool\Wrapper\EntityWrapper;

/**
 * Class TextSliderTranslationRepository
 * @package Escuela\SliderBundle\Entity
 */
class TextSliderTranslationRepository extends EntityRepository
{
    /**
     * Loads all translations with all translatable
     * fields from the given entity
     *
     * @param object $entity Must implement Translatable
     * @return array list of translations in locale groups
     */
    public function findTranslations($entity)
    {
        $result = array();
        $wrapped = new EntityWrapper($entity, $this->_em);
        if ($wrapped->hasValidIdentifier()) {
            $entityId = $wrapped->getIdentifier();

            $translationMeta = $this->getClassMetadata(); // table inheritance support
            $qb = $this->_em->createQueryBuilder();
            $qb->select('trans.content, trans.field, trans.locale')
                ->from($translationMeta->rootEntityName, 'trans')
                ->where('trans.object = :entityId')
                ->orderBy('trans.locale');
            $q = $qb->getQuery();

            $data = $q->execute( compact('entityId'), Query::HYDRATE_ARRAY);

            if ($data && is_array($data) && count($data)) {
                foreach ($data as $row) {
                    $result[$row['locale']][$row['field']] = $row['content'];
                }
            }
        }
        return $result;
    }
    public function findAllTranslations($entities){
        $result = array();
        foreach ($entities as $entity) {
            $result[] = array('entity' => $entity, 'traducciones' => $this->findTranslations($entity));
        }
        return $result;
    }

}
