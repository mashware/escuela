<?php

namespace Escuela\SliderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TextSlider
 *
 * @ORM\Table()
 * @ORM\Table(name="text_slider")
 * @ORM\Entity(repositoryClass="Escuela\SliderBundle\Entity\TextSliderRepository")
 * @Gedmo\TranslationEntity(class="Escuela\SliderBundle\Entity\TextSliderTranslation")
 */
class TextSlider
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Escuela\SliderBundle\Entity\Slider", inversedBy="text")
     *
     */
    private $slider;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @var integer
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="topCss", type="integer")
     */
    private $topCss;

    /**
     * @var integer
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="leftCss", type="integer")
     */
    private $leftCss;

    /**
     * @var integer
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="widthCss", type="integer")
     */
    private $widthCss;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    /**
     * @ORM\OneToMany(targetEntity="TextSliderTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    private $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function addTranslation(TextSliderTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slider
     *
     * @param \Escuela\SliderBundle\Entity\Slider $slider
     * @return $this
     */
    public function setSlider(\Escuela\SliderBundle\Entity\Slider $slider)
    {
        $this->slider = $slider;

        return $this;
    }

    /**
     * Get slider
     *
     * @return integer 
     */
    public function getSlider()
    {
        return $this->slider;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return TextSlider
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set topCss
     *
     * @param integer $topCss
     * @return TextSlider
     */
    public function setTopCss($topCss)
    {
        $this->topCss = $topCss;

        return $this;
    }

    /**
     * Get topCss
     *
     * @return integer 
     */
    public function getTopCss()
    {
        return $this->topCss;
    }

    /**
     * Set leftCss
     *
     * @param integer $leftCss
     * @return TextSlider
     */
    public function setLeftCss($leftCss)
    {
        $this->leftCss = $leftCss;

        return $this;
    }

    /**
     * Get leftCss
     *
     * @return integer 
     */
    public function getLeftCss()
    {
        return $this->leftCss;
    }

    /**
     * Set widthCss
     *
     * @param integer $widthCss
     * @return TextSlider
     */
    public function setWidthCss($widthCss)
    {
        $this->widthCss = $widthCss;

        return $this;
    }

    /**
     * Get widthCss
     *
     * @return integer 
     */
    public function getWidthCss()
    {
        return $this->widthCss;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getText();
    }
}
