<?php

namespace Escuela\SliderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
/**
 * Slider
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Escuela\SliderBundle\Entity\SliderRepository")
 */
class Slider
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Escuela\SliderBundle\Entity\SeccionSlider")
     * @ORM\JoinColumn(name="seccion_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @Assert\NotBlank()
     */
    private $seccion;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255)
     * @Assert\Length(min=4, max=255)
     */
    private $imagen;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     * @Assert\NotBlank()
     */
    private $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="state", type="boolean")
     * @Assert\Type(type="boolean")
     */
    private $state = false;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true))
     * @Assert\Url()
     */
    private $url = null;

    /**
     * @Assert\Image(maxSize = "1024k")
     */
    protected $foto;


    /**
     * @ORM\OneToMany(targetEntity="Escuela\SliderBundle\Entity\TextSlider", mappedBy="slider", cascade={"remove"})
     */
    protected $text;

    public function __construct()
    {
        $this->text = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set seccion
     *
     * @param \Escuela\SliderBundle\Entity\SeccionSlider $seccion
     * @return $this
     */
    public function setSeccion(\Escuela\SliderBundle\Entity\SeccionSlider $seccion)
    {
        $this->seccion = $seccion;

        return $this;
    }

    /**
     * Get seccion
     *
     * @return string 
     */
    public function getSeccion()
    {
        return $this->seccion;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Slider
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Slider
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set state
     *
     * @param boolean $state
     * @return Slider
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return boolean 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Slider
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getImagen();
    }
    public function getText(){
        return $this->text;
    }
    public function setText(\Escuela\SliderBundle\Entity\TextSlider $text){
        $this->text[] = $text;

        return $this;
    }

    /**
     * @param UploadedFile $foto
     */
    public function setFoto(UploadedFile $foto = null)
    {
        $this->foto = $foto;
    }

    /**
     * @return UploadedFile
     */
    public function getFoto()
    {
        return $this->foto;
    }
    public function subirFoto($directorioDestino)
    {
        if (null === $this->foto) {
            return;
        }

        $nombreArchivoFoto = uniqid('escuela-').'-slider.jpg';
        $this->foto->move($directorioDestino, $nombreArchivoFoto);

        $this->setImagen($nombreArchivoFoto);

    }
}
