/**
 * Created by Alberto on 4/05/14.
 */
$(document).ready(function(){
    $('body').on('click', '.edit-text-slider', function(e){
        e.preventDefault();

        $('#sliderText-new-es').remove();
        $('#sliderText-new-en').remove();

        var id = $(this).attr('data-id');
        var form = $('body').find('form#form-text-sliderText');

        form.find('input.texto').val('');
        form.find('input.width').val('');
        form.find('input.left').val('');
        form.find('input.top').val('');

        form.find('#escuela_sliderbundle_textslider_id').val(id);

        var tab = form.find('.tab-content').find('.active').find('.box-body');
        tab.append($('<div/>', {'class': 'overlay'}));
        tab.append($('<div/>', {'class': 'loading-img'}));

        $.get($(this).attr('href'), function(data){
            var espanol = JSON.parse(data['espanol']);
            var traducciones = data['translations'];
            var nameForm = $('#form-text-sliderText').attr('name');
            $.each(espanol, function(index, value){
                if(index != 'slider'){
                    $('#'+nameForm+'_'+campoSymfony(index)).val(value);
                }
            });
            if(Object.prototype.toString.call(traducciones) !== '[object Array]'){
                $.each(traducciones['en'], function(index, value){
                    $('#'+nameForm+'_'+index+'_ingles_'+index+'_en').val(value);
                });
            }
            tab.find('.overlay').remove();
            tab.find('.loading-img').remove();
        });
        form.find('button[type="submit"]').html('Actualizar');

    });
    /**
     * Construye el nombre del campo como hace symfony2
     * @param campo
     * @returns {string}
     */
    function campoSymfony(campo){
        var palabras = campo.split("_");
        var result = "";
        $.each(palabras, function(index, value){
            if(index>0){
                palabras[index] = value.replace(/^[a-z]/, function(m){ return m.toUpperCase() });
            }
            result += palabras[index];
        });
        return result;
    }


    $('#form-idiomas li a').click(function(e){
        var idioma = $(this).attr('data-idioma');
        $('.contenedor-slider').find('h1').addClass('oculto');
        $('.contenedor-slider').find('h1.'+idioma).removeClass('oculto');

    });
    function getTexto(){
        var form = $('body').find('form#form-text-sliderText');
        if($('#escuela_sliderbundle_textslider_id').val() == ""){
            var idioma = form.find('.nav-tabs-custom li.active a').attr('data-idioma');
            var idTexto = '#texto-new-idioma-'+idioma;
        }else{
            var idioma = $('.nav-tabs-custom').find('li.active a').attr('data-idioma');
            var idTexto = '#texto-'+form.find('#escuela_sliderbundle_textslider_id').val()+'-idioma-'+idioma;
        }
        return idTexto;
    }
    $('.width').change(function(e){
        var idTexto = getTexto();
        $(idTexto).css('width', $(this).val()+'px');
    });
    $('.left').change(function(e){
        var idTexto = getTexto();
        $(idTexto).css('left', $(this).val()+'px');
    });
    $('.top').change(function(e){
        var idTexto = getTexto();
        $(idTexto).css('top', $(this).val()+'px');
    });

    $('.texto').change(function(e){
        var texto = $(this).val();
        var form = $('body').find('form#form-text-sliderText');

        var idioma = form.find('.nav-tabs-custom li.active a').attr('data-idioma');
        var idNuevo = '#texto-new-idioma-'+idioma;
        var idTexto = getTexto();

        if($(idTexto).length){ //Si es uno ya guardado
            $(idTexto).html(texto);
        }else if(form.find('#escuela_sliderbundle_textslider_id').val()!="" && $('.contenedor-slider').find(idTexto).length==0){ //Si es uno guardado pero que no está el otro idioma
            var h1 = $('<h1/>', {'id': idTexto.replace('#',''), 'class': 'texto-slider '+idioma, 'style': 'top:0;left:0;'}).html(texto);
            $('.contenedor-slider').append(h1);
        }else if($('.contenedor-slider').find(idNuevo).length==0){ //Si es uno nuevo
            var h1 = $('<h1/>', {'id': idNuevo.replace('#',''), 'class': 'texto-slider '+idioma, 'style': 'top:0;left:0;'}).html(texto);
            $('.contenedor-slider').append(h1);
        }else{ //si es uno nuevo pero ya creado
            var elementoSlider = $('.contenedor-slider').find(idNuevo);
            elementoSlider.html(texto);
        }



        /*if(form.find('#escuela_sliderbundle_textslider_id').val()=="" && $('.contenedor-slider').find(idNuevo).length==0){
            //Añades uno nuevo
            var h1 = $('<h1/>', {'id': idNuevo.replace('#',''), 'class': 'texto-slider '+idioma, 'style': 'top:0;left:0;'}).html(texto);
            $('.contenedor-slider').append(h1);
        }else if($('.contenedor-slider').find(idNuevo).length){
            //Editas uno nuevo aún no guardado
            var elementoSlider = $('.contenedor-slider').find(idNuevo);
            elementoSlider.html(texto);
        }else{
            //Editas uno ya guardado
            var idTexto = getTexto();
            $(idTexto).html(texto);
        }*/
    });
    $('#form-text-sliderText').submit(function(e){
        e.preventDefault();
        var form = $(this);
        var tab = form.find('.tab-content').find('.active').find('.box-body');

        tab.append($('<div/>', {'class': 'overlay'}));
        tab.append($('<div/>', {'class': 'loading-img'}));

        $.post($(this).attr('action'), $(this).serialize(), function(data){
            if(data['state']){

                if(form.find('#escuela_sliderbundle_textslider_id').val()==""){
                    var fila = JSON.parse(data['fila']);

                    $('#texto-new-idioma-es').attr('id', 'texto-'+fila['id']+'-idioma-es');
                    $('#texto-new-idioma-en').attr('id', 'texto-'+fila['id']+'-idioma-en');

                    var tr = '<tr>'+
                        '<td>'+fila['id']+'</td>'+
                        '<td>'+fila['text']+'</td>'+
                        '<td>'+
                            '<a class="editar edit-text-slider" href="/app_dev.php/backend/slider/'+fila['id']+'/get/text"><i class="fa fa-pencil-square-o"></i> Editar</a>'+
                            '<a class="eliminar" href="/app_dev.php/slider/'+fila['id']+'/text/deleteForm"><i class="fa fa-times-circle"></i> Eliminar</a>'+
                        '</td>'+
                    '</tr>';
                    $('#listado-textos').append(tr);
                    form.find('input[type="number"]').val('');
                    form.find('input[type="text"]').val('');
                    $('#escuela_sliderbundle_textslider_id').val('');
                    form.find('button[type="submit"]').html('Añadir');
                }else{
                    alert('La consulta se ha realizado con exito');
                    var id = form.find('#escuela_sliderbundle_textslider_id').val();
                    var text = form.find('#escuela_sliderbundle_textslider_text').val();

                    var tr = $('#listado-textos').find('tr#fila_'+id);
                    tr.find('td:nth-child(2)').html(text);

                    form.find('input[type="number"]').val('');
                    form.find('input[type="text"]').val('');
                    $('#escuela_sliderbundle_textslider_id').val('');
                    form.find('button[type="submit"]').html('Añadir');
                }
            }else{
                alert('Se ha producido un error');
            }
            tab.find('.loading-img').remove();
            tab.find('.overlay').remove();
        });
    })
});