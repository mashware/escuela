<?php
namespace Escuela\FrontendBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Class Builder
 * @package Escuela\BackendBundle\Menu
 */
class BuilderFrontend extends ContainerAware
{
    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function escuelaMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'list-unstyled');

        $menu->addChild('Información', array('route' => 'frontend_escuela_estatica', 'routeParameters' => array('slug' => 'informacion-de-la-miguel-angel-jimenez-golf-academy')));
        $menu->addChild('Edades y grupos', array('route' => 'frontend_escuela_estatica', 'routeParameters' => array('slug' => 'edades-y-grupos')));
        $menu->addChild('Plan de trabajo', array('route' => 'frontend_escuela_estatica', 'routeParameters' => array('slug' => 'plan-de-trabajo')));
        $menu->addChild('Profesores', array('route' => 'frontend_escuela_profesores'));
        $menu->addChild('Instalaciones', array('route' => 'frontend_escuela_instalaciones'));
        $menu->addChild('Precios', array('route' => 'frontend_escuela_estatica', 'routeParameters' => array('slug' => 'precios')));
        $menu->addChild('Información para padres', array('route' => 'frontend_escuela_estatica', 'routeParameters' => array('slug' => 'informacion-para-padres')));
        $menu->addChild('V1 Academy', array('route' => 'frontend_escuela_estatica', 'routeParameters' => array('slug' => 'v1-golf-academy')));
        //$menu->addChild('Zona Jugadores', array('url' => '#'));

        return $menu;

    }

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function principalMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');

        $menu->addChild('Inicio', array('route' => 'frontend_homepage_idioma'));
        $menu->addChild('El campo', array('uri' => '#'))
            ->setAttribute('dropdown', true);
        $menu['El campo']->addChild('Recorrido', array('route' => 'frontend_elcampo'));
        $menu['El campo']->addChild('Tarifas', array('route' => 'frontend_tarifas'));
        $menu['El campo']->addChild('Ofertas', array('route' => 'frontend_ofertas'));
        $menu['El campo']->addChild('Videoclases', array('route' => 'frontend_videoclases'));

        $menu->addChild('Escuela de golf', array('route' => 'frontend_escuela_estatica', 'routeParameters' => array('slug' => 'informacion-de-la-miguel-angel-jimenez-golf-academy')));
        $menu->addChild('Competiciones', array('route' => 'frontend_competiciones'));
        $menu->addChild('Partner Club', array('route' => 'frontend_homepage_idioma'));
        $menu->addChild('Correspondencias', array('route' => 'frontend_correspondencias'));
        $menu->addChild('Donde estamos', array('route' => 'frontend_donde_estamos'));
       if($options['locale'] == 'es'){
            $menu->addChild('English', array('route' => 'frontend_homepage_idioma', 'routeParameters' => array('_locale' => 'en')))
                ->setAttribute('iconRight', 'icono-ingles');
        }else{
            $menu->addChild('Español', array('route' => 'frontend_homepage'))
                ->setAttribute('iconRight', 'icono-espana');
        }

        return $menu;
    }

    public function partnerMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Inicio', array('route' => 'frontend_escuela_estatica', 'routeParameters' => array('slug' => 'informacion-de-la-miguel-angel-jimenez-golf-academy')));
        $menu->addChild('Presentación', array('route' => 'frontend_partner_presentacion'));
        $menu->addChild('Comercios', array('route' => 'frontend_escuela_estatica', 'routeParameters' => array('slug' => 'plan-de-trabajo')));

        $em = $this->container->get('doctrine.orm.entity_manager');
        $partnerCategorias =   $em->getRepository('BackendBundle:PartnerCategorias')->findAll();

        foreach($partnerCategorias as $categoria){
            $menu['Comercios']->addChild($categoria->getNombre(), array('route' => 'frontend_partner_comercio', 'routeParameters' => array('categoriaSlug' => $categoria->getSlug())));
        }

        $menu->addChild('Solicitar mi tarjeta', array('route' => 'frontend_partner_tarjeta'));
        $menu->addChild('Únete a nosotros como PARTNER', array('route' => 'frontend_escuela_instalaciones'));

        return $menu;
    }
}