<?php
 
/**
 * Copyright (c) 2014 Mashware
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Feel free to edit as you please, and have fun.
 *
 * @author Alberto Vioque <mashware@gmail.com>
 */

namespace Escuela\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
/**
 * Class TarjetaType
 * @package Escuela\FrontendBundle\Form
 */
class TarjetaType extends AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text', array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),)
            ))
            ->add('apellidos', 'text', array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),)
            ))
            ->add('email', 'email', array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),)
            ))
            ->add('telefono', 'text', array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),)
            ))
            ->add('ciudad', 'text', array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),)
            ))
            ->add('recogida', 'choice', array(
                'choices' => array(
                    'club' => 'Recoger en el club ',
                    'casa' => 'Enviar a casa'
                ),
                'multiple' => false,
                'expanded' => true,
                'required' => true,
                'data' => 'club',
            ))
            ->add('direccion', 'text', array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),)
            ))
            ->add('cp', 'text', array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),)
            ))
            ->add('poblacion', 'text', array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),)
            ))
            ->add('provincia', 'text', array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 3)),)
            ));
    }

    public function getName()
    {
        return 'partner_tarjeta';
    }
}