<?php

namespace Escuela\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;

use Symfony\Component\HttpFoundation\JsonResponse;



/**
 * Class DefaultController
 * @package Escuela\FrontendBundle\Controller
 */
class DefaultController extends Controller
{

    public function publicidadAction(){
        $em = $this->getDoctrine()->getManager();

        $publicidad = $em->getRepository('BackendBundle:Publicidad')->findPublicadas();

        return $this->render( 'FrontendBundle:Default:publicidad.html.twig', array(
            'publicidad' => $publicidad
        ));
    }
    public function patrocinadoresAction(){
        $em = $this->getDoctrine()->getManager();

        $patrocinadores = $em->getRepository('BackendBundle:Patrocinadores')->findPatrocinadoresPublicados();

        return $this->render( 'FrontendBundle:Default:patrocinadores.html.twig', array(
            'patrocinadores' => $patrocinadores
        ));
    }
    /**
     * Muestra la portada
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sliders = $em->getRepository('SliderBundle:Slider')->findSliderActivos(1);
        $bloques = $em->getRepository('BackendBundle:Blocks')->findBlocksVisualizadosHome();
        $publicidad = $em->getRepository('BackendBundle:Publicidad')->findPublicadas();
        $patrocinadores = $em->getRepository('BackendBundle:Patrocinadores')->findPatrocinadoresPublicados();
        $limite = 4;
        $noticias = $em->getRepository('BackendBundle:Noticia')->findNoticiasEscuela($limite);

        return $this->render('FrontendBundle:Default:index.html.twig', array(
            'noticias' => $noticias,
            'sliders' => $sliders,
            'bloques' => $bloques,
            'publicidad' => $publicidad,
            'patrocinadores' => $patrocinadores
        ));
    }

    /**
     * Muestra la sección de El campo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function campoAction(){
        $em = $this->getDoctrine()->getManager();

        $hoyos = $em->getRepository('BackendBundle:Hoyos')->findAll(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("El campo");
        //End Breadcrumbs

        return $this->render('FrontendBundle:Default:elcampo.html.twig', array(
            'hoyos' => $hoyos
        ));
    }

    /**
     * Muestra la pantalla de Ofertas
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ofertasAction(){
        $em = $this->getDoctrine()->getManager();

        $ofertas = $em->getRepository('BackendBundle:Ofertas')->findOfertasActivas();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Ofertas");
        //End Breadcrumbs

        return $this->render('FrontendBundle:Default:ofertas.html.twig', array(
            'ofertas' => $ofertas
        ));
    }

    /**
     * Muestra el video del campo enviado
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function campoVideoAction($id){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Hoyos')->find($id);

        return $this->render('FrontendBundle:Default:elcampoVideo.html.twig', array(
            'hoyo' => $entity
        ));
    }

    /**
     * Muestra las imágenes del campo enviado
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function campoImagenesAction($id){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Hoyos')->find($id);

        $finder = new Finder();
        $urlBaseImagenes = $this->container->getParameter('uploads.hoyos.absoluto').'imagenes/'.$entity->getDestinoImagenes();
        $iterator = $finder->files()->in($urlBaseImagenes);

        $urlImagenes = $this->container->getParameter('uploads.hoyos.relativo').'imagenes/';

        return $this->render('FrontendBundle:Default:elcampoImagenes.html.twig', array(
            'hoyo' => $entity,
            'urlImagenes' => $urlImagenes,
            'iteratorImagenes' => $iterator
        ));
    }


    /**
     * Muestra la pantalla de competiciones
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function competicionesAction(){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Competiciones')->findAll(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Competiciones");
        //End Breadcrumbs

        return $this->render('FrontendBundle:Default:competiciones.html.twig', array(
            'competiciones' => $entity
        ));
    }

    /**
     * Muestra la pantalla de correspondencia
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function correspondenciasAction(){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Correspondencia')->findAll(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Correspondencias");
        //End Breadcrumbs

        return $this->render('FrontendBundle:Default:correspondencia.html.twig', array(
            'correspondencias' => $entity
        ));
    }

    /**
     * Muestra la pantalla de dondeEstamos
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dondeEstamosAction(){
        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Donde estamos");
        //End Breadcrumbs

        return $this->render('FrontendBundle:Default:dondeEstamos.html.twig');
    }

    /**
     * Muestra la información de un bloque enviado mediante slug
     *
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function informacionBloqueAction($slug){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Blocks')->findBySlug($slug);
        $bloques = $em->getRepository('BackendBundle:Blocks')->findBlocksVisualizadosHome();
        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem($entity['title']);
        //End Breadcrumbs

        return $this->render('FrontendBundle:Default:informacionBloque.html.twig', array(
            'informacion' => $entity,
            'bloques' => $bloques
        ));
    }

    /**
     * Visualiza una noticia enviada
     *
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function noticiasAction($slug){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Noticia')->findBySlug($slug);
        $limite = 5;
        $bloques = $em->getRepository('BackendBundle:Noticia')->findNoticiasEscuela($limite);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem('Noticias');
        //End Breadcrumbs

        return $this->render('FrontendBundle:Default:noticia.html.twig', array(
            'noticia' => $entity,
            'noticias' => $bloques
        ));
    }


    public function tarifasAction(){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Tarifa')->findAll(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem('Tarifas');
        //End Breadcrumbs

        return $this->render('FrontendBundle:Default:tarifas.html.twig', array(
            'tarifas' => $entity
        ));
    }
    public function circuitoNoticiasJsonAction($page){
        $em = $this->getDoctrine()->getManager();

        $noticias = $em->getRepository('BackendBundle:Noticia')->findNoticiasCircuito($page, 10);
        $response = new JsonResponse($noticias, '200', array('application/json'));

        return $response;
    }
    public function circuitoNoticiaJsonAction($slug){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Noticia')->findBySlug($slug);

        $response = new JsonResponse($entity, '200', array('application/json'));

        return $response;
    }
    public function paginaPoliticaAction(){
        return $this->render('FrontendBundle:Default:politicas.html.twig');
    }

    public function videoClasesAction(){
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('BackendBundle:VideoClases')->findAllQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('pagina', 1)/*page number*/,
            9/*limit per page*/
        );

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Galería de Videos");
        //End Breadcrumbs

        return $this->render('FrontendBundle:Default:videos.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    public function videoAction($id){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:VideoClases')->findOneBy(array('id' => $id));

        if (!$entity) {
            throw $this->createNotFoundException('El video ya no existe');
        }

        return $this->render('FrontendBundle:Default:video.html.twig', array(
            'entity' => $entity,
        ));
    }
}
