<?php

namespace Escuela\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class EscuelaController
 * @package Escuela\FrontendBundle\Controller
 */
class EscuelaController extends Controller
{
    public function escuelaAction(){
        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Escuela de golf");
        //End Breadcrumbs
        return $this->render('FrontendBundle:Escuela:escuela.html.twig');
    }
    /**
     * Muestra una página estática
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function estaticaAction($slug){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Paginas')->findBySlug($slug);

        return $this->render('FrontendBundle:Escuela:estatico.html.twig', array(
            'entity' => $entity,
        ));
    }
    public function profesoresAction(){
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Profesores')->findAll(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return $this->render('FrontendBundle:Escuela:profesores.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function instalacionesAction(){
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Instalaciones')->findAll(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        // Obtenemos todas las fotos de cada instalación
        $imagenes = array();
        foreach($entities as $entity){
            $finder = new Finder();
            $urlBaseImagenes = $this->container->getParameter('uploads.instalaciones.absoluto').'imagenes/'.$entity->getCarpeta();
            $imagenes [$entity->getId()]= $finder->files()->in($urlBaseImagenes);
        }


        return $this->render('FrontendBundle:Escuela:instalaciones.html.twig', array(
            'instalaciones' => $entities,
            'imagenes' => $imagenes
        ));
    }

}
