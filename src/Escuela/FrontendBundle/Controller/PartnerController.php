<?php

namespace Escuela\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Response;
use Escuela\FrontendBundle\Form\TarjetaType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PartnerController
 * @package Escuela\FrontendBundle\Controller
 */
class PartnerController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction(){
        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Partner Club");
        //End Breadcrumbs

        return $this->render('FrontendBundle:Partner:index.html.twig');
    }

    /**
     * @return Response
     */
    public function presentacionAction(){
        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Partner Club", $this->get("router")->generate("frontend_partner"));
        $breadcrumbs->addItem("Presentación");
        //End Breadcrumbs

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Partner')->findDestacados();


        return $this->render('FrontendBundle:Partner:presentacion.html.twig', array(
            'destacados' => $entity,
        ));

    }

    public function categoriaAction($categoriaSlug){
        $em = $this->getDoctrine()->getManager();

        $categoria = $em->getRepository('BackendBundle:PartnerCategorias')->findOneBy(array('slug' => $categoriaSlug));
        $entity = $em->getRepository('BackendBundle:Partner')->findByCategoria($categoriaSlug);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Partner Club", $this->get("router")->generate("frontend_partner"));
        $breadcrumbs->addItem($categoria->getNombre());
        //End Breadcrumbs

        return $this->render('FrontendBundle:Partner:categoria.html.twig', array(
            'partners' => $entity,
            'categoria' => $categoria
        ));

    }

    /**
     * @param $categoriaSlug
     * @param $partner
     * @return Response
     */
    public function partnerAction($categoriaSlug, $partner){
        $em = $this->getDoctrine()->getManager();

        $categoria = $em->getRepository('BackendBundle:PartnerCategorias')->findOneBy(array('slug' => $categoriaSlug));
        $entity = $em->getRepository('BackendBundle:Partner')->findBySlug($partner);

        $finder = new Finder();
        $urlBaseImagenes = $this->container->getParameter('uploads.partner.absoluto').'imagenes/'.$entity['imagenes'];
        $iterator = $finder->files()->in($urlBaseImagenes);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Partner Club", $this->get("router")->generate("frontend_partner"));
        $breadcrumbs->addItem($categoria->getNombre());
        //End Breadcrumbs

        return $this->render('FrontendBundle:Partner:partner.html.twig', array(
            'partner' => $entity,
            'categoria' => $categoria,
            'imagenes' => $iterator,
        ));

    }

    /**
     * @param $seleccionada
     * @param $imagenes
     * @return Response
     */

    public function visualizarImagenesAction($seleccionada, $imagenes){
        $finder = new Finder();
        $urlBaseImagenes = $this->container->getParameter('uploads.partner.absoluto').'imagenes/'.$imagenes.'/';
        $iterator = $finder->files()->in($urlBaseImagenes);

        return $this->render('FrontendBundle:Partner:imagenes.html.twig', array(
            'imagenes' => $iterator,
            'seleccionada' => $seleccionada,
            'carpeta' => $imagenes
        ));
    }

    public function tarjetaAction(Request $request){
        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Partner Club", $this->get("router")->generate("frontend_partner"));
        $breadcrumbs->addItem('Solicitar mi tarjeta Partner Club');
        //End Breadcrumbs

        $form = $this->createTarjetaForm();
        $form->handleRequest($request);

        if($form->isValid()){
            $message = \Swift_Message::newInstance()
                ->setSubject('Tarjeta Partner')
                ->setFrom('prueba@mashware.es')
                ->setTo('mashware@gmail.com')
                ->setBody(
                    $this -> renderView ('FrontendBundle:Partner:email.html.twig', array (
                        'datos' => $form->getData()
                    ))
                )
            ;
            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('frontend_partner_tarjeta_ok'));
        }
        return $this->render('FrontendBundle:Partner:tarjeta.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
    * @return \Symfony\Component\Form\Form
    */
    private function createTarjetaForm()
    {
        $editForm = $this->createForm(new TarjetaType(), array(), array(
            'action' => $this->generateUrl('frontend_partner_tarjeta'),
            'method' => 'PUT',
        ));
        $editForm->add('submit', 'submit', array('label' => 'Pedir mi tarjeta'));

        return $editForm;
    }
    public function tarjetaOkAction(){
        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Inicio", $this->get("router")->generate("frontend_homepage_idioma"));
        $breadcrumbs->addItem("Partner Club", $this->get("router")->generate("frontend_partner"));
        $breadcrumbs->addItem('Solicitar mi tarjeta Partner Club');
        //End Breadcrumbs

        return $this->render('FrontendBundle:Partner:tarjetaOk.html.twig');
    }
}
