<?php
namespace Escuela\BackendBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Class Builder
 * @package Escuela\BackendBundle\Menu
 */
class Builder extends ContainerAware
{
    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function leftMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'sidebar-menu');

        $menu->addChild('Dashboard', array('route' => 'backend_dashboard'))
            ->setAttribute('icon', 'dashboard');

        //Pagina
        $menu->addChild('Página Web',array(
                'route' => 'backend_dashboard_slug',
                'routeParameters' => array('slug' => 'pagina-web'),
                'class' => 'treeview'))
            ->setAttribute('icon', 'home');
        $menu['Página Web']->addChild('Bloques', array('route' => 'backend_block'))
            ->setAttribute('icon', 'angle-double-right');
        $menu['Página Web']->addChild('Competiciones', array('route' => 'backend_competiciones'))
            ->setAttribute('icon', 'angle-double-right');
        $menu['Página Web']->addChild('Correspondencias', array('route' => 'correspondencia'))
            ->setAttribute('icon', 'angle-double-right');
        $menu['Página Web']->addChild('El campo', array('route' => 'el-campo'))
            ->setAttribute('icon', 'angle-double-right');
        $menu['Página Web']->addChild('Ofertas', array('route' => 'backend_ofertas'))
            ->setAttribute('icon', 'angle-double-right');
        $menu['Página Web']->addChild('Patrocinadores', array('route' => 'backend_patrocinadores'))
            ->setAttribute('icon', 'angle-double-right');
        $menu['Página Web']->addChild('Sponsors', array('route' => 'backend_publicidad'))
            ->setAttribute('icon', 'angle-double-right');
        $menu['Página Web']->addChild('Slider', array('route' => 'slider'))
            ->setAttribute('icon', 'angle-double-right');
        //$menu['Página Web']->addChild('Tarifas', array('route' => 'slider'))
            //->setAttribute('icon', 'angle-double-right');

        $menu['Página Web']->addChild('Tarifas', array('route' => 'backend_tarifa'))
            ->setAttribute('icon', 'angle-double-right');
        $menu['Página Web']->addChild('Videoclases', array('route' => 'backend_videoclases'))
            ->setAttribute('icon', 'angle-double-right');

        //Escuela
        $menu->addChild('Escuela', array(
            'route' => 'backend_dashboard_slug',
            'routeParameters' => array('slug' => 'escuela'),
            'class' => 'treeview'))
            ->setAttribute('icon', 'graduation-cap');
        $menu['Escuela']->addChild('Instalaciones', array('route' => 'backend_instalaciones'))
            ->setAttribute('icon', 'angle-double-right');
        $menu['Escuela']->addChild('Páginas', array('route' => 'backend_paginas'))
            ->setAttribute('icon', 'angle-double-right');
        $menu['Escuela']->addChild('Profesores', array('route' => 'backend_profesores'))
            ->setAttribute('icon', 'angle-double-right');

        //Noticias
        $menu->addChild('Noticias', array('route' => 'backend_noticia', 'class' => 'treeview'))
            ->setAttribute('icon', 'rss');
        // Partner
        $menu->addChild('Partner',array(
            'route' => 'backend_noticia',
            'routeParameters' => array('slug' => 'partner'),
            'class' => 'treeview'))
            ->setAttribute('icon', 'home');

        $em = $this->container->get('doctrine.orm.entity_manager');
        $partnerCategorias =   $em->getRepository('BackendBundle:PartnerCategorias')->findAll();

        foreach($partnerCategorias as $categoria){
            $menu['Partner']->addChild($categoria->getNombre(), array('route' => 'backend_partner', 'routeParameters' => array('categoria' => $categoria->getSlug())))
                ->setAttribute('icon', 'angle-double-right');
        }

        //Escuela
        $menu->addChild('Tienda', array(
            'route' => 'backend_dashboard_slug',
            'routeParameters' => array('slug' => 'tienda'),
            'class' => 'treeview'))
            ->setAttribute('icon', 'graduation-cap');
        $menu['Tienda']->addChild('Productos', array('route' => 'backend_dashboard_productos'))
            ->setAttribute('icon', 'angle-double-right');
        $menu['Tienda']->addChild('Ventas', array('route' => 'backend_paginas'))
            ->setAttribute('icon', 'angle-double-right');

        return $menu;
    }
}