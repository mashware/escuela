<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\Patrocinadores;
use Escuela\BackendBundle\Form\PatrocinadoresType;

/**
 * Patrocinadores controller.
 *
 */
class PatrocinadoresController extends Controller
{

    /**
     * Lists all Patrocinadores entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Patrocinadores')->findAll();
        $patrocinadoresPublicados = $em->getRepository('BackendBundle:Patrocinadores')->findPatrocinadoresPublicados();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Patrocinadores");
        //End Breadcrumbs

        return $this->render('BackendBundle:Patrocinadores:index.html.twig', array(
            'entities' => $entities,
            'patrocinadores' => $patrocinadoresPublicados
        ));
    }
    /**
     * Creates a new Patrocinadores entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Patrocinadores();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.patrocinadores.absoluto').'imagenes/';

        //Subimos las imágenes
        $upload = $this->get('uploads');
        $upload->setFiles($formRequest['foto']);
        $upload->setDirectorio($directorioBase);
        $archivosSubidos = $upload->up();

        $entity->setImagen($archivosSubidos->first());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_patrocinadores'));

        }elseif($entity->getImagen()){
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
        }

        return $this->render('BackendBundle:Patrocinadores:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Patrocinadores entity.
    *
    * @param Patrocinadores $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Patrocinadores $entity)
    {
        $form = $this->createForm(new PatrocinadoresType(), $entity, array(
            'action' => $this->generateUrl('backend_patrocinadores_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Patrocinadores entity.
     *
     */
    public function newAction()
    {
        $entity = new Patrocinadores();
        $form   = $this->createCreateForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Patrocinadores", $this->get("router")->generate("backend_patrocinadores"));
        $breadcrumbs->addItem("Nuevo");
        //End Breadcrumbs

        return $this->render('BackendBundle:Patrocinadores:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Patrocinadores entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Patrocinadores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Patrocinadores entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Patrocinadores", $this->get("router")->generate("backend_patrocinadores"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Patrocinadores:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Patrocinadores entity.
    *
    * @param Patrocinadores $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Patrocinadores $entity)
    {
        $form = $this->createForm(new PatrocinadoresType(), $entity, array(
            'action' => $this->generateUrl('backend_patrocinadores_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Patrocinadores entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Patrocinadores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Patrocinadores entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $imagenEnBBDD = $entity->getImagen();
        $directorioBase = $this->container->getParameter('uploads.patrocinadores.absoluto').'imagenes/';
        $formRequest = $request->files->get($editForm->getName());

        if($formRequest['foto']){
            //Subimos las imágenes
            $upload = $this->get('uploads');
            $upload->setFiles($formRequest['foto']);
            $upload->setDirectorio($directorioBase);

            $archivosSubidos = $upload->up();
            $entity->setImagen($archivosSubidos->first());
        }
        if ($editForm->isValid()) {
            $em->flush();
            if(file_exists($directorioBase.$imagenEnBBDD) && $formRequest['foto']){
                unlink($directorioBase.$imagenEnBBDD);
            }
            return $this->redirect($this->generateUrl('backend_patrocinadores'));
        }elseif(file_exists($directorioBase.$entity->getImagen())){
            unlink($directorioBase.$entity->getImagen());
        }

        return $this->render('BackendBundle:Patrocinadores:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a Patrocinadores entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Patrocinadores')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Patrocinadores entity.');
            }
            $directorioBase = $this->container->getParameter('uploads.patrocinadores.absoluto').'imagenes/';

            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_patrocinadores'));
    }

    /**
     * Creates a form to delete a Patrocinadores entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_patrocinadores_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Patrocinadores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Patrocinadores entity.');
        }
        $form = $this->createDeleteForm($entity->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Patrocinadores", $this->get("router")->generate("backend_patrocinadores"));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Patrocinadores:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }
}
