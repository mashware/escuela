<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\SubcategoriaTarifa;
use Escuela\BackendBundle\Form\SubcategoriaTarifaType;

/**
 * SubcategoriaTarifa controller.
 *
 */
class SubcategoriaTarifaController extends Controller
{

    /**
     * Lists all SubcategoriaTarifa entities.
     *
     */
    public function indexAction($id_categoria)
    {
        $em = $this->getDoctrine()->getManager();

        $categoria = $em->getRepository('BackendBundle:CategoriaTarifa')->findCategoria($id_categoria);
        $entities = $em->getRepository('BackendBundle:SubcategoriaTarifa')->findBy(array('categoria' => $id_categoria));

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Categorías de Tarifas", $this->get("router")->generate("backend_categorias_tarifas"));
        $breadcrumbs->addItem($categoria->getNombre());
        $breadcrumbs->addItem("Subcategorías");
        //End Breadcrumbs

        return $this->render('BackendBundle:SubcategoriaTarifa:index.html.twig', array(
            'entities' => $entities,
            'categoria' => $categoria
        ));
    }
    /**
     * Creates a new SubcategoriaTarifa entity.
     *
     */
    public function createAction(Request $request, $id_categoria)
    {
        $entity = new SubcategoriaTarifa();

        $em = $this->getDoctrine()->getManager();
        $categoria = $em->getRepository('BackendBundle:CategoriaTarifa')->findCategoria($id_categoria);

        $entity->setCategoria($categoria);

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_subcategorias_tarifas', array('id_categoria' => $id_categoria, 'id' => $entity->getId())));
        }

        return $this->render('BackendBundle:SubcategoriaTarifa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a SubcategoriaTarifa entity.
    *
    * @param SubcategoriaTarifa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(SubcategoriaTarifa $entity)
    {
        $form = $this->createForm(new SubcategoriaTarifaType(), $entity, array(
            'action' => $this->generateUrl('backend_subcategorias_create', array('id_categoria' => $entity->getCategoria()->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SubcategoriaTarifa entity.
     *
     */
    public function newAction($id_categoria)
    {
        $entity = new SubcategoriaTarifa();

        $em = $this->getDoctrine()->getManager();
        $categoria = $em->getRepository('BackendBundle:CategoriaTarifa')->findCategoria($id_categoria);

        $entity->setCategoria($categoria);

        $form   = $this->createCreateForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Categorías de Tarifas", $this->get("router")->generate("backend_categorias_tarifas"));
        $breadcrumbs->addItem($categoria->getNombre());
        $breadcrumbs->addItem("Subcategorías", $this->get("router")->generate("backend_subcategorias_tarifas", array('id_categoria' => $categoria->getId())));
        $breadcrumbs->addItem("Nuevo");
        //End Breadcrumbs

        return $this->render('BackendBundle:SubcategoriaTarifa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'categoria' => $categoria
        ));
    }

    /**
     * Displays a form to edit an existing SubcategoriaTarifa entity.
     *
     */
    public function editAction($id, $id_categoria)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:SubcategoriaTarifa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubcategoriaTarifa entity.');
        }

        $editForm = $this->createEditForm($entity, $id_categoria);
        $categoria = $entity->getCategoria();
        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Categorías de Tarifas", $this->get("router")->generate("backend_categorias_tarifas"));
        $breadcrumbs->addItem($categoria->getNombre());
        $breadcrumbs->addItem("Subcategorías", $this->get("router")->generate("backend_subcategorias_tarifas", array('id_categoria' => $categoria->getId())));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:SubcategoriaTarifa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'categoria' => $entity->getCategoria()
        ));
    }

    /**
     * Creates a form to edit a SubcategoriaTarifa entity.
     *
     * @param SubcategoriaTarifa $entity
     * @param $id_categoria
     * @return \Symfony\Component\Form\Form
     */
    private function createEditForm(SubcategoriaTarifa $entity, $id_categoria)
    {
        $form = $this->createForm(new SubcategoriaTarifaType(), $entity, array(
            'action' => $this->generateUrl('backend_subcategorias_update', array('id' => $entity->getId(), 'id_categoria' => $id_categoria)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing SubcategoriaTarifa entity.
     *
     */
    public function updateAction(Request $request, $id, $id_categoria)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:SubcategoriaTarifa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubcategoriaTarifa entity.');
        }

        $editForm = $this->createEditForm($entity, $id_categoria);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('backend_subcategorias_edit', array('id' => $id, 'id_categoria' => $id_categoria)));
        }

        return $this->render('BackendBundle:SubcategoriaTarifa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'categoria' => $entity->getCategoria()
        ));
    }
    /**
     * Deletes a SubcategoriaTarifa entity.
     *
     */
    public function deleteAction(Request $request, $id, $id_categoria)
    {
        $form = $this->createDeleteForm($id, $id_categoria);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:SubcategoriaTarifa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SubcategoriaTarifa entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_subcategorias'));
    }

    /**
     * Creates a form to delete a SubcategoriaTarifa entity by id.
     *
     * @param $id
     * @param $id_categoria
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm($id, $id_categoria)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_subcategorias_delete', array('id' => $id, 'id_categoria' => $id_categoria)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @param $id_categoria
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id, $id_categoria){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:SubcategoriaTarifa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Subcategoria entity.');
        }
        $form = $this->createDeleteForm($entity->getId(), $id_categoria);
        $categoria = $entity->getCategoria();
        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Categorías de Tarifas", $this->get("router")->generate("backend_categorias_tarifas"));
        $breadcrumbs->addItem($categoria->getNombre());
        $breadcrumbs->addItem("Subcategorías", $this->get("router")->generate("backend_subcategorias_tarifas", array('id_categoria' => $categoria->getId())));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('BackendBundle:SubcategoriaTarifa:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }
}
