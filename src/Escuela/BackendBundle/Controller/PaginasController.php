<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\Paginas;
use Escuela\BackendBundle\Form\PaginasType;

/**
 * Paginas controller.
 *
 */
class PaginasController extends Controller
{

    /**
     * Lists all Paginas entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Paginas')->findAll();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Páginas");
        //End Breadcrumbs

        return $this->render('BackendBundle:Paginas:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Paginas entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Paginas();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_paginas'));
        }

        return $this->render('BackendBundle:Paginas:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Paginas entity.
     *
     * @param Paginas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Paginas $entity)
    {
        $form = $this->createForm(new PaginasType(), $entity, array(
            'action' => $this->generateUrl('backend_paginas_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Paginas entity.
     *
     */
    public function newAction()
    {
        $entity = new Paginas();
        $form   = $this->createCreateForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Páginas", $this->get("router")->generate("backend_paginas"));
        $breadcrumbs->addItem("Crear");
        //End Breadcrumbs

        return $this->render('BackendBundle:Paginas:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Paginas entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Paginas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Paginas entity.');
        }

        $editForm = $this->createEditForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Páginas", $this->get("router")->generate("backend_paginas"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Paginas:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Paginas entity.
    *
    * @param Paginas $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Paginas $entity)
    {
        $form = $this->createForm(new PaginasType(), $entity, array(
            'action' => $this->generateUrl('backend_paginas_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Paginas entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Paginas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Paginas entity.');
        }
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('backend_paginas'));
        }

        return $this->render('BackendBundle:Paginas:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a Paginas entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Paginas')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Paginas entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_paginas'));
    }

    /**
     * Creates a form to delete a Paginas entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_paginas_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Paginas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Páginas entity.');
        }
        $form = $this->createDeleteForm($entity->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Páginas", $this->get("router")->generate("backend_paginas"));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Paginas:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }
}
