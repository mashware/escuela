<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\Ofertas;
use Escuela\BackendBundle\Form\OfertasType;

/**
 * Ofertas controller.
 *
 */
class OfertasController extends Controller
{

    /**
     * Lists all Ofertas entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Ofertas')->findAll();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Ofertas");
        //End Breadcrumbs

        return $this->render('BackendBundle:Ofertas:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Ofertas entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Ofertas();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.ofertas.absoluto').'imagenes/';

        //Subimos las imágenes
        $upload = $this->get('uploads');
        $upload->setFiles($formRequest['foto']);
        $upload->setDirectorio($directorioBase);
        $archivosSubidos = $upload->up();

        $entity->setImagen($archivosSubidos->first());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_ofertas'));
        }elseif($entity->getImagen()){
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
        }

        return $this->render('BackendBundle:Ofertas:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Ofertas entity.
    *
    * @param Ofertas $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Ofertas $entity)
    {
        $form = $this->createForm(new OfertasType(), $entity, array(
            'action' => $this->generateUrl('backend_ofertas_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Ofertas entity.
     *
     */
    public function newAction()
    {
        $entity = new Ofertas();
        $form   = $this->createCreateForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Ofertas", $this->get("router")->generate("backend_ofertas"));
        $breadcrumbs->addItem("Nueva");
        //End Breadcrumbs

        return $this->render('BackendBundle:Ofertas:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Ofertas entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Ofertas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ofertas entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Ofertas", $this->get("router")->generate("backend_ofertas"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Ofertas:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Ofertas entity.
    *
    * @param Ofertas $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Ofertas $entity)
    {
        $form = $this->createForm(new OfertasType(), $entity, array(
            'action' => $this->generateUrl('backend_ofertas_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Ofertas entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Ofertas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ofertas entity.');
        }

        $imagenEnBBDD = $entity->getImagen();
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $formRequest = $request->files->get($editForm->getName());
        $directorioBase = $this->container->getParameter('uploads.ofertas.absoluto').'imagenes/';

        if($formRequest['foto']){

            //Subimos las imágenes
            $upload = $this->get('uploads');
            $upload->setFiles($formRequest['foto']);
            $upload->setDirectorio($directorioBase);

            $archivosSubidos = $upload->up();

            $entity->setImagen($archivosSubidos->first());
        }

        if ($editForm->isValid()) {
            $em->flush();
            if(file_exists($directorioBase.$imagenEnBBDD) && $formRequest['foto']){
                unlink($directorioBase.$imagenEnBBDD);
            }
            return $this->redirect($this->generateUrl('backend_ofertas_edit', array('id' => $id)));
        }elseif(file_exists($directorioBase.$entity->getImagen())){
            unlink($directorioBase.$entity->getImagen());
        }

        return $this->render('BackendBundle:Ofertas:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a Ofertas entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Ofertas')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Ofertas entity.');
            }

            $em->remove($entity);
            $em->flush();

            $directorioBase = $this->container->getParameter('uploads.ofertas.absoluto').'imagenes/';
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
        }

        return $this->redirect($this->generateUrl('backend_ofertas'));
    }

    /**
     * Creates a form to delete a Ofertas entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_ofertas_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Ofertas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ofertas entity.');
        }
        $form = $this->createDeleteForm($entity->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Ofertas", $this->get("router")->generate("backend_ofertas"));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Ofertas:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }
}
