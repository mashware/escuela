<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\CategoriaTarifa;
use Escuela\BackendBundle\Form\CategoriaTarifaType;

/**
 * CategoriaTarifa controller.
 *
 */
class CategoriaTarifaController extends Controller
{

    /**
     * Lists all CategoriaTarifa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:CategoriaTarifa')->findAll();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Categorias de Tarifas");
        //End Breadcrumbs

        return $this->render('BackendBundle:CategoriaTarifa:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new CategoriaTarifa entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new CategoriaTarifa();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_categorias_tarifas', array('id' => $entity->getId())));
        }

        return $this->render('BackendBundle:CategoriaTarifa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a CategoriaTarifa entity.
    *
    * @param CategoriaTarifa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(CategoriaTarifa $entity)
    {
        $form = $this->createForm(new CategoriaTarifaType(), $entity, array(
            'action' => $this->generateUrl('backend_categorias_tarifas_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CategoriaTarifa entity.
     *
     */
    public function newAction()
    {
        $entity = new CategoriaTarifa();
        $form   = $this->createCreateForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Categorías de Tarifas", $this->get("router")->generate("backend_categorias_tarifas"));
        $breadcrumbs->addItem("Nueva");
        //End Breadcrumbs

        return $this->render('BackendBundle:CategoriaTarifa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CategoriaTarifa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:CategoriaTarifa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CategoriaTarifa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Categorías de Tarifas", $this->get("router")->generate("backend_categorias_tarifas"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:CategoriaTarifa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a CategoriaTarifa entity.
    *
    * @param CategoriaTarifa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CategoriaTarifa $entity)
    {
        $form = $this->createForm(new CategoriaTarifaType(), $entity, array(
            'action' => $this->generateUrl('backend_categorias_tarifas_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CategoriaTarifa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:CategoriaTarifa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CategoriaTarifa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('backend_categorias_tarifas', array('id' => $id)));
        }

        return $this->render('BackendBundle:CategoriaTarifa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Deletes a CategoriaTarifa entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:CategoriaTarifa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CategoriaTarifa entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_categorias_tarifas'));
    }

    /**
     * Creates a form to delete a CategoriaTarifa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_categorias_tarifas_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:CategoriaTarifa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categoria entity.');
        }
        $form = $this->createDeleteForm($entity->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Home");
        $breadcrumbs->addItem("Categorías de Tarifas", $this->get("router")->generate("backend_categorias_tarifas"));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('BackendBundle:CategoriaTarifa:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }
}
