<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Escuela\BackendBundle\Entity\Publicidad;
use Escuela\BackendBundle\Form\PublicidadType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;



/**
 * Class PublicidadController
 * @package Escuela\BackendBundle\Controller
 */
class PublicidadController extends Controller
{
    /**
     * Listado de la publicidad
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $listado = $em->getRepository('BackendBundle:Publicidad')->findAll();
        $publicidadPublicada = $em->getRepository('BackendBundle:Publicidad')->findPublicadas();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Publicidad");
        //End Breadcrumbs

        return $this->render('BackendBundle:Publicidad:index.html.twig', array(
            'listado' => $listado,
            'publicidadPublicada' => $publicidadPublicada
        ));
    }

    /**
     * Crea el formulario de inserción
     *
     * @param Publicidad $publicidad
     * @return \Symfony\Component\Form\Form
     */
    private function createNewForm(Publicidad $publicidad){
        $form = $this->createForm(new PublicidadType(), $publicidad, array(
            'action' => $this->generateUrl('backend_publicidad_create'),
            'method' => 'POST'
            ));
        $form->add('submit', 'submit', array('label' => 'Añadir'));

        return $form;
    }

    /**
     * Muestra el formulario de creación
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(){
        $publicidad = new Publicidad();

        $form = $this->createNewForm($publicidad);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Publicidad", $this->get("router")->generate("backend_publicidad"));
        $breadcrumbs->addItem("Nuevo");
        //End Breadcrumbs

        return $this->render('BackendBundle:Publicidad:new.html.twig', array(
            'publicidad' => $publicidad,
            'form' => $form->createView()
        ));
    }

    /**
     * Crea la publicidad
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request){
        $publicidad = new Publicidad();

        $form = $this->createNewForm($publicidad);
        $form->handleRequest($request);

        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.publicidad.absoluto').'imagenes/';

        //Subimos las imágenes
        $upload = $this->get('uploads');
        $upload->setFiles($formRequest['foto']);
        $upload->setDirectorio($directorioBase);

        $archivosSubidos = $upload->up();
        $publicidad->setImagen($archivosSubidos->first());

        $upload->setFiles($formRequest['fotoTitulo']);
        $archivosSubidos = $upload->up();
        $publicidad->setImagenTitulo($archivosSubidos->first());

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();

            $em->persist($publicidad);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_publicidad'));
        }else{
            if($publicidad->getImagen() && file_exists($this->container->getParameter('uploads.publicidad.absoluto').$publicidad->getImagen())){
                unlink($this->container->getParameter('uploads.publicidad.absoluto').$publicidad->getImagen());
            }
            if($publicidad->getImagenTitulo() && file_exists($this->container->getParameter('uploads.publicidad.absoluto').$publicidad->getImagenTitulo())){
                unlink($this->container->getParameter('uploads.publicidad.absoluto').$publicidad->getImagenTitulo());
            }
        }

        return $this->render('BackendBundle:Publicidad:new.html.twig', array(
            'publicidad' => $publicidad,
            'form' => $form->createView()
        ));
    }

    /**
     * Genera el formulario de edición
     *
     * @param Publicidad $publicidad
     * @return \Symfony\Component\Form\Form
     */
    private function createEditForm(Publicidad $publicidad){
        $form = $this->createForm(new PublicidadType(), $publicidad, array(
            'action' => $this->generateUrl('backend_publicidad_update', array('id' => $publicidad->getId())),
            'method' => "put"
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }

    /**
     * Muestra el fomulario de edición
     *
     * @ParamConverter ("publicidad", class="BackendBundle:Publicidad")
     *
     * @param Publicidad $publicidad
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editAction(Publicidad $publicidad){
        if(!$publicidad){
            throw $this->createNotFoundException("La entidad que intentas editar no existe");
        }

        $form = $this->createEditForm($publicidad);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Publicidad", $this->get("router")->generate("backend_publicidad"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Publicidad:edit.html.twig', array(
            'publicidad' => $publicidad,
            'edit_form' => $form->createView()
        ));
    }

    /**
     * Edita la publicidad
     *
     * @ParamConverter ("publicidad", class="BackendBundle:Publicidad")
     *
     * @param Request $request
     * @param Publicidad $publicidad
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function updateAction(Request $request, Publicidad $publicidad){
        if(!$publicidad){
            throw $this->createNotFoundException("La entidad que intentas editar no existe");
        }

        $formEdit = $this->createEditForm($publicidad);
        $formEdit->handleRequest($request);

        $imagenEnBBDDTitulo = $publicidad->getImagenTitulo();
        $imagenEnBBDD = $publicidad->getImagen();

        $formRequest = $request->files->get($formEdit->getName());

        $directorioBase = $this->container->getParameter('uploads.publicidad.absoluto').'imagenes/';
        $upload = $this->get('uploads');
        $upload->setDirectorio($directorioBase);

        if($formRequest['foto']){
            //Subimos las imágenes
            $upload->setFiles($formRequest['foto']);
            $archivosSubidos = $upload->up();
            $publicidad->setImagen($archivosSubidos->first());
        }
        if($formRequest['fotoTitulo']){
            //Subimos las imágenes
            $upload->setFiles($formRequest['fotoTitulo']);
            $archivosSubidos = $upload->up();
            $publicidad->setImagenTitulo($archivosSubidos->first());
        }

        if($formEdit->isValid()){
            $em = $this->getDoctrine()->getManager();

            $em->persist($publicidad);
            $em->flush();
            if(file_exists($directorioBase.$imagenEnBBDD) && $formRequest['foto']){
                unlink($directorioBase.$imagenEnBBDD);
            }
            if(file_exists($directorioBase.$imagenEnBBDDTitulo) && $formRequest['fotoTitulo']){
                unlink($directorioBase.$imagenEnBBDDTitulo);
            }
            return $this->redirect($this->generateUrl('backend_publicidad'));
        }else{
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$publicidad->getImagen());
            }
            if(file_exists($directorioBase.$publicidad->getImagenTitulo())){
                unlink($directorioBase.$publicidad->getImagenTitulo());
            }
        }

        return $this->render('BackendBundle:Publicidad:edit.html.twig', array(
            'publicidad' => $publicidad,
            'form' => $form->createView()
        ));
    }

    /**
     * Crea el formulario de eliminar una publicidad
     *
     * @param Publicidad $publicidad
     * @return \Symfony\Component\Form\Form
     */
    public function createDeleteForm(Publicidad $publicidad){
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_publicidad_delete', array('id' => $publicidad->getId())))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Muestra el formulario de eliminación
     *
     * @ParamConverter ("publicidad", class="BackendBundle:Publicidad")
     *
     * @param Publicidad $publicidad
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction(Publicidad $publicidad){
        if(!$publicidad){
            throw $this->createNotFoundException('El elemento que desea borrar no existe');
        }
        $form = $this->createDeleteForm($publicidad);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Publicidad", $this->get("router")->generate("backend_publicidad"));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Publicidad:remove.html.twig', array(
            'entity' => $publicidad,
            'delete_form' => $form->createView()
        ));
    }

    /**
     * Elimina la publicidad enviada
     *
     * @ParamConverter ("publicidad", class="BackendBundle:Publicidad")
     *
     * @param Request $request
     * @param Publicidad $publicidad
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction(Request $request, Publicidad $publicidad){
        if(!$publicidad){
            throw $this->createNotFoundException('El elemento que intentas eliminar no existe');
        }
        $form = $this->createDeleteForm($publicidad);
        $form->handleRequest($request);
        $directorioBase = $this->container->getParameter('uploads.publicidad.absoluto').'imagenes/';

        if($form->isValid()){
            if(file_exists($directorioBase.$publicidad->getImagen())){
                unlink($directorioBase.$publicidad->getImagen());
            }
            if(file_exists($directorioBase.$publicidad->getImagenTitulo())){
                unlink($directorioBase.$publicidad->getImagenTitulo());
            }

            $em = $this->getDoctrine()->getManager();

            $em->remove($publicidad);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_publicidad'));
    }
}
