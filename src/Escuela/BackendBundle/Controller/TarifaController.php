<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\Tarifa;
use Escuela\BackendBundle\Entity\TarifaTranslation;
use Escuela\BackendBundle\Form\TarifaType;
use Escuela\BackendBundle\Form\TarifaPDFUploadType;

/**
 * Tarifa controller.
 *
 */
class TarifaController extends Controller
{

    /**
     * Lists all Tarifa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Tarifa')->findAll();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Tarifas");
        //End Breadcrumbs
        $formUploadPdf = $this->createPDFUploadForm();

        return $this->render('BackendBundle:Tarifa:index.html.twig', array(
            'entities' => $entities,
            'pdf_form' => $formUploadPdf->createView()
        ));
    }
    /**
     * Creates a new Tarifa entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Tarifa();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.tarifas.absoluto').'imagenes/';
        //Subimos las imágenes
        $upload = $this->get('uploads');
        $upload->setDirectorio($directorioBase);
        $upload->setFiles($formRequest['foto']);
        $archivosSubidos = $upload->up();
        $entity->setImagen($archivosSubidos->first());
        //En ingles
        $upload->setFiles($formRequest['foto_ingles']);
        $archivosSubidos = $upload->up();
        $entity->addTranslation(new TarifaTranslation('en', 'imagen', $archivosSubidos->first()));

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_tarifa', array('id' => $entity->getId())));
        }elseif($entity->getImagen()){
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
            if(file_exists($directorioBase.$archivosSubidos->first())){
                unlink($directorioBase.$archivosSubidos->first());
            }
        }

        return $this->render('BackendBundle:Tarifa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Tarifa entity.
     *
     * @param Tarifa $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Tarifa $entity)
    {
        $form = $this->createForm(new TarifaType(), $entity, array(
            'action' => $this->generateUrl('backend_tarifa_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Tarifa entity.
     *
     */
    public function newAction()
    {
        $entity = new Tarifa();
        $form   = $this->createCreateForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Tarifas", $this->get("router")->generate("backend_tarifa"));
        $breadcrumbs->addItem("Crear");
        //End Breadcrumbs

        return $this->render('BackendBundle:Tarifa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Tarifa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Tarifa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tarifa entity.');
        }

        $editForm = $this->createEditForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Tarifas", $this->get("router")->generate("backend_tarifa"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Tarifa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Tarifa entity.
    *
    * @param Tarifa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Tarifa $entity)
    {
        $form = $this->createForm(new TarifaType(), $entity, array(
            'action' => $this->generateUrl('backend_tarifa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Tarifa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Tarifa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tarifa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        // Imagen
        $imagenEnBBDD = $entity->getImagen();
        $imagenEnBBDDIngles = $em->getRepository('BackendBundle:Tarifa')->findImagenIdioma($entity, 'en');
        $imagenEnBBDDInglesNombre = $imagenEnBBDDIngles->getContent();

        $directorioBase = $this->container->getParameter('uploads.tarifas.absoluto').'imagenes/';
        $formRequest = $request->files->get($editForm->getName());

        if($formRequest['foto']){
            //Subimos las imágenes
            $upload = $this->get('uploads');
            $upload->setFiles($formRequest['foto']);
            $upload->setDirectorio($directorioBase);

            $archivosSubidos = $upload->up();
            $entity->setImagen($archivosSubidos->first());
        }
        if($formRequest['foto_ingles']){
            //Subimos las imágenes
            $upload = $this->get('uploads');
            $upload->setFiles($formRequest['foto_ingles']);
            $upload->setDirectorio($directorioBase);

            $archivosSubidos = $upload->up();
            $imagenEnBBDDIngles->setContent($archivosSubidos->first());
            $entity->addTranslation($imagenEnBBDDIngles);
        }

        if ($editForm->isValid()) {
            $em->flush();
            if(file_exists($directorioBase.$imagenEnBBDD) && $formRequest['foto']){
                unlink($directorioBase.$imagenEnBBDD);
            }
            if(file_exists($directorioBase.$imagenEnBBDDInglesNombre) && $formRequest['foto_ingles']){
                unlink($directorioBase.$imagenEnBBDDInglesNombre);
            }
            return $this->redirect($this->generateUrl('backend_tarifa', array('id' => $id)));
        }else{
            if(file_exists($directorioBase.$imagenEnBBDD)){
                unlink($directorioBase.$imagenEnBBDD);
            }
            if(file_exists($directorioBase.$imagenEnBBDDIngles->getContent())){
                unlink($directorioBase.$imagenEnBBDDIngles->getContent());
            }
        }

        return $this->render('BackendBundle:Tarifa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a Tarifa entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Tarifa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tarifa entity.');
            }

            $directorioBase = $this->container->getParameter('uploads.tarifas.absoluto').'imagenes/';

            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }

            $imagenEnBBDDIngles = $em->getRepository('BackendBundle:Tarifa')->findImagenIdioma($entity, 'en');

            if(file_exists($directorioBase.$imagenEnBBDDIngles->getContent())){
                unlink($directorioBase.$imagenEnBBDDIngles->getContent());
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_tarifa'));
    }

    /**
     * Creates a form to delete a Tarifa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_tarifa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Tarifa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tarifa entity.');
        }
        $form = $this->createDeleteForm($entity->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Tarifas", $this->get("router")->generate("backend_tarifa"));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Ofertas:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }
    /**
     * Creates a form to update pdf tarifas.
     *
     * @param Tarifa $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createPDFUploadForm()
    {
        $form = $this->createForm(new TarifaPDFUploadType(), null, array(
            'action' => $this->generateUrl('backend_tarifa_upload_pdf'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Upload'));

        return $form;
    }
    public function uploadPdfAction(Request $request){
        $pdfForm = $this->createPDFUploadForm();
        $pdfForm->handleRequest($request);

        $formRequest = $request->files->get($pdfForm->getName());
        $directorio = $this->container->getParameter('uploads.tarifas.absoluto').'pdf/';
        if ($pdfForm->isValid()) {
            if($formRequest ['pdf']){
                //Subimos las pdf
                $upload = $this->get('uploads');
                $upload->setFiles($formRequest ['pdf']);
                $upload->setDirectorio($directorio);

                $upload->up('tarifas.pdf');
            }
            if($formRequest['pdf_ingles']){
                //Subimos las pdf
                $upload = $this->get('uploads');
                $upload->setFiles($formRequest['pdf_ingles']);
                $upload->setDirectorio($directorio);

                $upload->up('tarifas_en.pdf');
            }

            return $this->redirect($this->generateUrl('backend_tarifa'));
        }
    }
}
