<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\Correspondencia;
use Escuela\BackendBundle\Form\CorrespondenciaType;

/**
 * Correspondencia controller.
 *
 */
class CorrespondenciaController extends Controller
{

    /**
     * Lists all Correspondencia entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Correspondencia')->findAll();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Correspondencias");
        //End Breadcrumbs

        return $this->render('BackendBundle:Correspondencia:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Correspondencia entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Correspondencia();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.correspondencia.absoluto').'imagenes/';

        //Subimos las imágenes
        $upload = $this->get('uploads');
        $upload->setFiles($formRequest['foto']);
        $upload->setDirectorio($directorioBase);
        $archivosSubidos = $upload->up();

        $entity->setImagen($archivosSubidos->first());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('correspondencia'));
        }elseif($entity->getImagen()){
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
        }

        return $this->render('BackendBundle:Correspondencia:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Correspondencia entity.
    *
    * @param Correspondencia $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Correspondencia $entity)
    {
        $form = $this->createForm(new CorrespondenciaType(), $entity, array(
            'action' => $this->generateUrl('correspondencia_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Correspondencia entity.
     *
     */
    public function newAction()
    {
        $entity = new Correspondencia();
        $form   = $this->createCreateForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Correspondencias", $this->get("router")->generate("correspondencia"));
        $breadcrumbs->addItem("Nuevo");
        //End Breadcrumbs

        return $this->render('BackendBundle:Correspondencia:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Correspondencia entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Correspondencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Correspondencia entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Correspondencias", $this->get("router")->generate("correspondencia"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Correspondencia:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Correspondencia entity.
    *
    * @param Correspondencia $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Correspondencia $entity)
    {
        $form = $this->createForm(new CorrespondenciaType(), $entity, array(
            'action' => $this->generateUrl('correspondencia_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Correspondencia entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Correspondencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Correspondencia entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $imagenEnBBDD = $entity->getImagen();
        $formRequest = $request->files->get($editForm->getName());
        $directorioBase = $this->container->getParameter('uploads.correspondencia.absoluto').'imagenes/';

        if($formRequest['foto']){
            //Subimos las imágenes
            $upload = $this->get('uploads');
            $upload->setFiles($formRequest['foto']);
            $upload->setDirectorio($directorioBase);

            $archivosSubidos = $upload->up();

            $entity->setImagen($archivosSubidos->first());
        }
        if ($editForm->isValid()) {
            $em->flush();
            if(file_exists($directorioBase.$imagenEnBBDD) && $formRequest['foto']){
                unlink($directorioBase.$imagenEnBBDD);
            }

            return $this->redirect($this->generateUrl('correspondencia', array('id' => $id)));
        }elseif(file_exists($directorioBase.$entity->getImagen())){
            unlink($directorioBase.$entity->getImagen());
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('BackendBundle:Correspondencia:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Correspondencia entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Correspondencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Correspondencia entity.');
        }

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($entity);
            $em->flush();

            $directorioBase = $this->container->getParameter('uploads.correspondencia.absoluto').'imagenes/';
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
        }

        return $this->redirect($this->generateUrl('correspondencia'));
    }

    /**
     * Creates a form to delete a Correspondencia entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('correspondencia_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Correspondencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Correspondencia entity.');
        }
        $form = $this->createDeleteForm($entity->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Correspondencias", $this->get("router")->generate("correspondencia"));
        $breadcrumbs->addItem("Eliminar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Correspondencia:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }
}
