<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\VideoClases;
use Escuela\BackendBundle\Form\VideoClasesType;

/**
 * VideoClases controller.
 *
 */
class VideoClasesController extends Controller
{

    /**
     * Lists all VideoClases entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('BackendBundle:VideoClases')->findAllQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('pagina', 1)/*page number*/,
            20/*limit per page*/
        );

        $this->getBreadcrumb("Listado");

        return $this->render('BackendBundle:VideoClases:index.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $entity = new VideoClases();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        // Subimos imagenes
        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.videoclases.absoluto').'imagenes/';

        $upload = $this->get('uploads');
        // Imagen principal
        $upload->setFiles($formRequest['imagen']);
        $upload->setDirectorio($directorioBase);
        $archivosSubidos = $upload->up();

        $entity->setImagen($archivosSubidos->first());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_videoclases', array('id' => $entity->getId())));
        }elseif(file_exists($directorioBase.$entity->getImagen())){
            unlink($directorioBase.$entity->getImagen());
        }

        // BreadCrumb
        $this->getBreadcrumb('Nuevo');
        ////////////

        return $this->render('BackendBundle:VideoClases:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a VideoClases entity.
     *
     * @param VideoClases $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(VideoClases $entity)
    {
        $form = $this->createForm(new VideoClasesType(), $entity, array(
            'action' => $this->generateUrl('backend_videoclases_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new VideoClases entity.
     *
     */
    public function newAction()
    {
        $entity = new VideoClases();
        $form   = $this->createCreateForm($entity);

        // BreadCrumb
        $this->getBreadcrumb('Nuevo');
        ////////////

        return $this->render('BackendBundle:VideoClases:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing VideoClases entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:VideoClases')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find VideoClases entity.');
        }

        $editForm = $this->createEditForm($entity);

        // BreadCrumb
        $this->getBreadcrumb('Editar');
        ////////////

        return $this->render('BackendBundle:VideoClases:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a VideoClases entity.
    *
    * @param VideoClases $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(VideoClases $entity)
    {
        $form = $this->createForm(new VideoClasesType(), $entity, array(
            'action' => $this->generateUrl('backend_videoclases_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing VideoClases entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:VideoClases')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find VideoClases entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        // Subimos imagenes
        $formRequest = $request->files->get($editForm->getName());
        $directorioBase = $this->container->getParameter('uploads.videoclases.absoluto').'imagenes/';
        $imagenBBDD = $entity->getImagen();

        if($formRequest['imagen']){
            $upload = $this->get('uploads');

            $upload->setFiles($formRequest['imagen']);
            $upload->setDirectorio($directorioBase);
            $archivosSubidos = $upload->up();

            $entity->setImagen($archivosSubidos->first());
        }

        if ($editForm->isValid()) {
            $em->flush();

            if(file_exists($directorioBase.$imagenBBDD) && $formRequest['imagen']){
                unlink($directorioBase.$imagenBBDD);
            }

            return $this->redirect($this->generateUrl('backend_videoclases_edit', array('id' => $id)));

        }elseif($imagenBBDD != $entity->getImagen() && file_exists($directorioBase.$entity->getImagen())){
            unlink($directorioBase.$entity->getImagen());
        }
        // BreadCrumb
        $this->getBreadcrumb('Editar');
        ////////////
        return $this->render('BackendBundle:VideoClases:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a VideoClases entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:VideoClases')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find VideoClases entity.');
            }

            $em->remove($entity);
            $em->flush();

            $directorioBase = $this->container->getParameter('uploads.videoclases.absoluto').'imagenes/';
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
        }

        return $this->redirect($this->generateUrl('backend_videoclases'));
    }

    /**
     * Creates a form to delete a VideoClases entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_videoclases_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:VideoClases')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Video Clases entity.');
        }
        $form = $this->createDeleteForm($entity->getId());

        // BreadCrumb
        $this->getBreadcrumb('Eliminar');
        ////////////

        return $this->render('BackendBundle:VideoClases:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }
    /**
     * Devuelve el breadcrumb para editar/ver/eliminar
     * @param $textoFinal
     */
    private function getBreadcrumb($textoFinal){
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Videoclases", $this->get("router")->generate("backend_videoclases"));
        $breadcrumbs->addItem($textoFinal);
    }
}
