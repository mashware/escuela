<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\Profesores;
use Escuela\BackendBundle\Form\ProfesoresType;

/**
 * Profesores controller.
 *
 */
class ProfesoresController extends Controller
{

    /**
     * Lists all Profesores entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Profesores')->findAll();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Profesores");
        //End Breadcrumbs

        return $this->render('BackendBundle:Profesores:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Profesores entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Profesores();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.profesores.absoluto').'imagenes/';

        //Subimos las imágenes
        $upload = $this->get('uploads');
        $upload->setFiles($formRequest['foto']);
        $upload->setDirectorio($directorioBase);

        $archivosSubidos = $upload->up();

        $entity->setImagen($archivosSubidos->first());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_profesores'));
        }elseif($entity->getImagen()){
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
        }

        return $this->render('BackendBundle:Profesores:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Profesores entity.
     *
     * @param Profesores $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Profesores $entity)
    {
        $form = $this->createForm(new ProfesoresType(), $entity, array(
            'action' => $this->generateUrl('backend_profesores_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Profesores entity.
     *
     */
    public function newAction()
    {
        $entity = new Profesores();
        $form   = $this->createCreateForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Profesores", $this->get("router")->generate("backend_profesores"));
        $breadcrumbs->addItem("Crear");
        //End Breadcrumbs

        return $this->render('BackendBundle:Profesores:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Profesores entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Profesores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Profesores entity.');
        }

        $editForm = $this->createEditForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Profesores", $this->get("router")->generate("backend_profesores"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Profesores:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Profesores entity.
    *
    * @param Profesores $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Profesores $entity)
    {
        $form = $this->createForm(new ProfesoresType(), $entity, array(
            'action' => $this->generateUrl('backend_profesores_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Profesores entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Profesores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Profesores entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $imagenEnBBDD = $entity->getImagen();
        $directorioBase = $this->container->getParameter('uploads.profesores.absoluto').'imagenes/';
        $formRequest = $request->files->get($editForm->getName());

        if($formRequest['foto']){
            //Subimos las imágenes
            $upload = $this->get('uploads');
            $upload->setFiles($formRequest['foto']);
            $upload->setDirectorio($directorioBase);

            $archivosSubidos = $upload->up();
            $entity->setImagen($archivosSubidos->first());
        }

        if ($editForm->isValid()) {
            $em->flush();
            if(file_exists($directorioBase.$imagenEnBBDD) && $formRequest['foto']){
                unlink($directorioBase.$imagenEnBBDD);
            }
            return $this->redirect($this->generateUrl('backend_profesores'));
        }elseif(file_exists($directorioBase.$entity->getImagen())){
            unlink($directorioBase.$entity->getImagen());
        }
        return $this->render('BackendBundle:Profesores:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a Profesores entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Profesores')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Profesores entity.');
            }
            $directorioBase = $this->container->getParameter('uploads.profesores.absoluto').'imagenes/';

            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_profesores'));
    }

    /**
     * Creates a form to delete a Profesores entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_profesores_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Profesores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Profesores entity.');
        }
        $form = $this->createDeleteForm($entity->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Profesores", $this->get("router")->generate("backend_profesores"));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Profesores:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }
}
