<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\Competiciones;
use Escuela\BackendBundle\Form\CompeticionesType;
use Escuela\BackendBundle\Form\CompeticionesPdfType;

/**
 * Competiciones controller.
 *
 */
class CompeticionesController extends Controller
{

    /**
     * Lists all Competiciones entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Competiciones')->findAll();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Competiciones");
        //End Breadcrumbs

        return $this->render('BackendBundle:Competiciones:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Competiciones entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Competiciones();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.competiciones.absoluto').'imagenes/';

        //Subimos las imágenes
        $upload = $this->get('uploads');
        $upload->setFiles($formRequest['foto']);
        $upload->setDirectorio($directorioBase);
        $archivosSubidos = $upload->up();

        $entity->setImagen($archivosSubidos->first());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_competiciones'));
        }elseif($entity->getImagen()){
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }
        }

        return $this->render('BackendBundle:Competiciones:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Competiciones entity.
    *
    * @param Competiciones $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Competiciones $entity)
    {
        $form = $this->createForm(new CompeticionesType(), $entity, array(
            'action' => $this->generateUrl('backend_competiciones_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Competiciones entity.
     *
     */
    public function newAction()
    {
        $entity = new Competiciones();
        $form   = $this->createCreateForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Competiciones", $this->get("router")->generate("backend_competiciones"));
        $breadcrumbs->addItem("Nuevo");
        //End Breadcrumbs

        return $this->render('BackendBundle:Competiciones:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Competiciones entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Competiciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Competiciones entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Competiciones", $this->get("router")->generate("backend_competiciones"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Competiciones:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Competiciones entity.
    *
    * @param Competiciones $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Competiciones $entity)
    {
        $form = $this->createForm(new CompeticionesType(), $entity, array(
            'action' => $this->generateUrl('backend_competiciones_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Competiciones entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Competiciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Competiciones entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $formRequest = $request->files->get($editForm->getName());
        $imagenEnBBDD = $entity->getImagen();
        $directorioBase = $this->container->getParameter('uploads.competiciones.absoluto').'imagenes/';

        if($formRequest['foto']){
            //Subimos las imágenes
            $upload = $this->get('uploads');
            $upload->setFiles($formRequest['foto']);
            $upload->setDirectorio($directorioBase);

            $archivosSubidos = $upload->up();

            $entity->setImagen($archivosSubidos->first());
        }

        if ($editForm->isValid()) {
            $em->flush();
            if(file_exists($directorioBase.$imagenEnBBDD) && $formRequest['foto']){
                unlink($directorioBase.$imagenEnBBDD);
            }
            return $this->redirect($this->generateUrl('backend_competiciones'));
        }elseif(file_exists($directorioBase.$entity->getImagen())){
            unlink($directorioBase.$entity->getImagen());
        }

        return $this->render('BackendBundle:Competiciones:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a Competiciones entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Competiciones')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Competiciones entity.');
            }
            $directorioBase = $this->container->getParameter('uploads.competiciones.absoluto');
            if(file_exists($directorioBase.'imagenes/'.$entity->getImagen())){
                unlink($directorioBase.'imagenes/'.$entity->getImagen());
            }
            if(file_exists($directorioBase.'pdf/es/circular_'.$entity->getId().'.pdf')){
                unlink($directorioBase.'pdf/es/circular_'.$entity->getId().'.pdf');
            }
            if(file_exists($directorioBase.'pdf/es/ranking_'.$entity->getId().'.pdf')){
                unlink($directorioBase.'pdf/es/ranking_'.$entity->getId().'.pdf');
            }
            if(file_exists($directorioBase.'pdf/en/circular_'.$entity->getId().'.pdf')){
                unlink($directorioBase.'pdf/en/circular_'.$entity->getId().'.pdf');
            }
            if(file_exists($directorioBase.'pdf/en/ranking_'.$entity->getId().'.pdf')){
                unlink($directorioBase.'pdf/en/ranking_'.$entity->getId().'.pdf');
            }
            if(file_exists($directorioBase.'pdf/en/horarios_'.$entity->getId().'.pdf')){
                unlink($directorioBase.'pdf/en/horarios_'.$entity->getId().'.pdf');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_competiciones'));
    }

    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Competiciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Competiciones entity.');
        }
        $form = $this->createDeleteForm($entity->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Competiciones", $this->get("router")->generate("backend_competiciones"));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Competiciones:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }

    /**
     * Creates a form to delete a Competiciones entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_competiciones_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    public function pdfAction($id){

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Competiciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Competiciones entity.');
        }

        $pdfForm = $this->createPdfForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Competiciones", $this->get("router")->generate("backend_competiciones"));
        $breadcrumbs->addItem($entity->getNombre());
        $breadcrumbs->addItem("Subir Pdf");
        //End Breadcrumbs

        return $this->render('BackendBundle:Competiciones:pdf.html.twig', array(
            'entity'      => $entity,
            'pdf_form'   => $pdfForm->createView(),
        ));
    }

    private function createPdfForm($entity)
    {
        $form = $this->createForm(new CompeticionesPdfType(), $entity, array(
            'action' => $this->generateUrl('backend_competiciones_pdf_upload', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Upload'));

        return $form;
    }

    public function pdfUploadAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Competiciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Competiciones entity.');
        }


        $pdfForm = $this->createPdfForm($entity);
        $pdfForm->handleRequest($request);

        $formRequest = $request->files->get($pdfForm->getName());
        $idioma = $request->get($pdfForm->getName());
        $idioma = $idioma['idioma'];
        $directorio = $this->container->getParameter('uploads.competiciones.absoluto').'pdf/'.$idioma.'/';

        if ($pdfForm->isValid()) {
            if($formRequest ['circular']){

                //Subimos las pdf
                $upload = $this->get('uploads');
                $upload->setFiles($formRequest ['circular']);
                $upload->setDirectorio($directorio);

                $upload->up('circular_'.$entity->getId().'.pdf');
            }
            if($formRequest['ranking']){
                //Subimos las pdf
                $upload = $this->get('uploads');
                $upload->setFiles($formRequest['ranking']);
                $upload->setDirectorio($directorio);

                $upload->up('ranking_'.$entity->getId().'.pdf');

            }
            if($formRequest['horarios']){
                //Subimos las pdf
                $upload = $this->get('uploads');
                $upload->setFiles($formRequest['horarios']);
                $upload->setDirectorio($directorio);

                $upload->up('horarios_'.$entity->getId().'.pdf');

            }
            return $this->redirect($this->generateUrl('backend_competiciones', array('id' => $id)));
        }

        return $this->render('BackendBundle:Competiciones:pdf.html.twig', array(
            'entity'      => $entity,
            'pdf_form'   => $pdfForm->createView(),
        ));
    }
}
