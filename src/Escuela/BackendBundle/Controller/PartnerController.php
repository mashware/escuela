<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\Partner;
use Escuela\BackendBundle\Form\PartnerType;

use Escuela\BackendBundle\Utiles\Util;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

/**
 * Partner controller.
 *
 */
class PartnerController extends Controller
{

    /**
     * Lists all Partner entities.
     *
     * @param $categoria
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($categoria)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Partner')->findAllByCategoriaSlug($categoria);
        $categoria = $em->getRepository('BackendBundle:PartnerCategorias')->findBySlug($categoria, \Doctrine\ORM\Query::HYDRATE_OBJECT);

        // BreadCrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Partners", $this->get("router")->generate("backend_dashboard_partner"));
        $breadcrumbs->addItem($categoria->getNombre());
        ////////////

        return $this->render('BackendBundle:Partner:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Partner entity.
     *
     */
    public function createAction(Request $request)
    {
        $categoria = $request->get('categoria');
        $entity = new Partner();
        $form = $this->createCreateForm($entity, $request->get('categoria'));
        $form->handleRequest($request);

        $entity->setImagenes(uniqid());

        $em = $this->getDoctrine()->getManager();
        $categoriaObj = $em->getRepository('BackendBundle:PartnerCategorias')->findBySlug($categoria, \Doctrine\ORM\Query::HYDRATE_OBJECT);
        $entity->setCategoria($categoriaObj);

        // Subimos imagenes
        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.partner.absoluto').'imagenes/';

        $upload = $this->get('uploads');
        // Imagen principal
        if($formRequest['principal']){
            $upload->setFiles($formRequest['principal']);
            $upload->setDirectorio($directorioBase);
            $archivosSubidos = $upload->up();
            $entity->setImagenPrincipal($archivosSubidos->first());
        }
        // Restos de imágenes
        if($formRequest['fotos'][0]){
            $upload->setDirectorio($directorioBase.$entity->getImagenes().'/');
            $upload->setFiles($formRequest['fotos']);
            $archivosSubidos = $upload->up();
        }
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_partner_show', array('id' => $entity->getId(), 'categoria' => $request->get('categoria'))));
        }

        if(file_exists($directorioBase.$entity->getImagenes().'/')){
            //Eliminamos las imágenes
            $fs = new Filesystem();
            $fs->remove($directorioBase.$entity->getImagenes().'/');
        }
        if(file_exists($directorioBase.$entity->getImagenPrincipal())){
            unlink($directorioBase.$entity->getImagenPrincipal());
        }

        // BreadCrumb
        $breadcrumbs = $this->getBreadcrumb($categoria, 'Nuevo');
        ////////////

        return $this->render('BackendBundle:Partner:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Partner entity.
     *
     * @param Partner $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Partner $entity, $categoria)
    {
        $form = $this->createForm(new PartnerType(), $entity, array(
            'action' => $this->generateUrl('backend_partner_create', array('categoria' => $categoria)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Partner entity.
     *
     */
    public function newAction($categoria)
    {
        $entity = new Partner();
        $form   = $this->createCreateForm($entity, $categoria);

        // BreadCrumb
        $breadcrumbs = $this->getBreadcrumb($categoria, 'Nuevo');
        ////////////

        return $this->render('BackendBundle:Partner:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Partner entity.
     *
     */
    public function showAction($id, $categoria)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Partner')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partner entity.');
        }
        $finder = new Finder();
        $urlBaseImagenes = $this->container->getParameter('uploads.partner.absoluto').'imagenes/'.$entity->getImagenes();
        $iterator = $finder->files()->in($urlBaseImagenes);

        // BreadCrumb
        $breadcrumbs = $this->getBreadcrumb($categoria, 'Ver');
        ////////////

        return $this->render('BackendBundle:Partner:show.html.twig', array(
            'entity'      => $entity,
            'imagenes'    => $iterator
        ));
    }

    /**
     * Displays a form to edit an existing Partner entity.
     *
     */
    public function editAction($id, $categoria)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Partner')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partner entity.');
        }

        $editForm = $this->createEditForm($entity, $categoria);
        $deleteForm = $this->createDeleteForm($id, $categoria);

        // BreadCrumb
        $breadcrumbs = $this->getBreadcrumb($categoria, 'Editar');
        ////////////

        return $this->render('BackendBundle:Partner:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Partner entity.
    *
    * @param Partner $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Partner $entity, $categoria)
    {
        $form = $this->createForm(new PartnerType(), $entity, array(
            'action' => $this->generateUrl('backend_partner_update', array(
                'id' => $entity->getId(),
                'categoria' => $categoria)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Partner entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $categoria = $request->get('categoria');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Partner')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partner entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $categoria);
        $editForm = $this->createEditForm($entity, $categoria);
        $editForm->handleRequest($request);


        // Subimos imagenes
        $formRequest = $request->files->get($editForm->getName());
        $directorioBase = $this->container->getParameter('uploads.partner.absoluto').'imagenes/';

        $upload = $this->get('uploads');
        // Imagen principal
        if($formRequest['principal']){
            $upload->setFiles($formRequest['principal']);
            $upload->setDirectorio($directorioBase);
            $archivosSubidos = $upload->up();
            $imgagenBBDDPrincipal = $entity->getImagenPrincipal();
            $entity->setImagenPrincipal($archivosSubidos->first());
        }
        // Restos de imágenes
        if($formRequest['fotos'][0]){
            $upload->setDirectorio($directorioBase.$entity->getImagenes().'/');
            $upload->setFiles($formRequest['fotos']);
            $fotos = $upload->up();
        }

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('backend_partner_edit', array(
                'id' => $id,
                'categoria' => $request->get('categoria')
            )));
        }
        if(file_exists($directorioBase.$entity->getImagenes().'/')){
            //Eliminamos las imágenes
            foreach($fotos as $foto){
                unlink($directorioBase.$entity->getImagenes().'/'.$foto);
            }
        }
        if(file_exists($directorioBase.$entity->getImagenPrincipal())){
            if($imgagenBBDDPrincipal != $entity->getImagenPrincipal()){
                unlink($directorioBase.$entity->getImagenPrincipal());
            }
        }

        // BreadCrumb
        $breadcrumbs = $this->getBreadcrumb($categoria, 'Editar');
        ////////////

        return $this->render('BackendBundle:Partner:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Partner entity.
     *
     * @param Request $request
     * @param $id
     * @param $categoria
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction(Request $request, $id, $categoria)
    {
        $form = $this->createDeleteForm($id, $categoria);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Partner')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Partner entity.');
            }

            //Eliminamos las imágenes
            $fs = new Filesystem();
            $directorioBase = $this->container->getParameter('uploads.partner.absoluto').'imagenes/';
            $fs->remove($directorioBase.$entity->getImagenes().'/');
            if(file_exists($directorioBase.$entity->getImagenPrincipal())){
                unlink($directorioBase.$entity->getImagenPrincipal());
            }

            //Borramos la entidad
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_partner'));
    }

    /**
     * Creates a form to delete a Partner entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $categoria)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_partner_delete', array('id' => $id, 'categoria' => $categoria)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id, $categoria){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Partner')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partner entity.');
        }
        $form = $this->createDeleteForm($entity->getId(), $categoria);

        // BreadCrumb
        $breadcrumbs = $this->getBreadcrumb($categoria, 'Eliminar');
        ////////////

        return $this->render('BackendBundle:Partner:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }
    /**
     * Elimina la imágen enviada
     *
     * @param  $id         Id del Partner
     * @param  $categoria  Categoria del Partner
     * @param  $imagen     Imagen a borrar
     * @return Response    Respuesta true/false
     */
    public function deleteImagenAction($id, $categoria, $imagen){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Partner')->find($id);

        if (!$entity) {
            return new Response("false");
        }

        $directorioBase = $this->container->getParameter('uploads.partner.absoluto').'imagenes/'.$entity->getImagenes().'/';
        if(file_exists($directorioBase.$imagen)){
            unlink($directorioBase.$imagen);
        }
        return new Response("true");
    }

    /**
     * Devuelve el breadcrumb para editar/ver/eliminar
     * @param $categoria  Slug de la categoria al que pertenece
     * @param $textoFinal Texto del final del slug
     */
    private function getBreadcrumb($categoria, $textoFinal){

        $em = $this->getDoctrine()->getManager();

        $categoria = $em->getRepository('BackendBundle:PartnerCategorias')->findBySlug($categoria, \Doctrine\ORM\Query::HYDRATE_OBJECT);

        // BreadCrumb
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Partners", $this->get("router")->generate("backend_dashboard_partner"));
        $breadcrumbs->addItem($categoria->getNombre(), $this->get("router")->generate("backend_partner", array('categoria' => $categoria->getSlug())));
        $breadcrumbs->addItem($textoFinal);
        ////////////
    }
}
