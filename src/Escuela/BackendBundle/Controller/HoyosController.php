<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\Hoyos;
use Escuela\BackendBundle\Form\HoyosType;

use Escuela\BackendBundle\Utiles\Util;

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;
/**
 * Hoyos controller.
 *
 */
class HoyosController extends Controller
{

    /**
     * Lists all Hoyos entities.
     *
     */
    public function indexAction()
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Hoyos");

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Hoyos')->findAll();

        return $this->render('BackendBundle:Hoyos:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Hoyos entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Hoyos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $entity->setDestinoImagenes(Util::getSlug($entity->getNombre()));

        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.hoyos.absoluto').'imagenes/'.$entity->getDestinoImagenes().'/';

        if ($form->isValid()) {
            //Subimos las imágenes
            $upload = $this->get('uploads');
            $upload->setFiles($formRequest['fotos']);
            $upload->setDirectorio($directorioBase);
            $archivosSubidos = $upload->up();
            $entity->setPrincipal($archivosSubidos->first());

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('el-campo_show', array('id' => $entity->getId())));
        }elseif(file_exists($directorioBase)){
            //Eliminamos las imágenes
            $fs = new Filesystem();
            $fs->remove($directorioBase);
        }

        return $this->render('BackendBundle:Hoyos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));

    }

    /**
    * Creates a form to create a Hoyos entity.
    *
    * @param Hoyos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Hoyos $entity)
    {
        $form = $this->createForm(new HoyosType(), $entity, array(
            'action' => $this->generateUrl('el-campo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Hoyos entity.
     *
     */
    public function newAction()
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("El campo", $this->get("router")->generate("el-campo"));
        $breadcrumbs->addItem("Crear");

        $entity = new Hoyos();
        $form   = $this->createCreateForm($entity);

        return $this->render('BackendBundle:Hoyos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Hoyos entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Hoyos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Hoyos entity.');
        }

        $finder = new Finder();
        $urlBaseImagenes = $this->container->getParameter('uploads.hoyos.absoluto').'imagenes/'.$entity->getDestinoImagenes();
        $iterator = $finder->files()->in($urlBaseImagenes);

        $urlImages = $this->container->getParameter('uploads.hoyos.relativo').'imagenes/';

        $deleteForm = $this->createDeleteForm($id);


        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("El campo", $this->get("router")->generate("el-campo"));
        $breadcrumbs->addItem("Ver");

        return $this->render('BackendBundle:Hoyos:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'imagenes'    => $iterator,
            'url_imagenes' => $urlImages,
        ));
    }

    /**
     * Displays a form to edit an existing Hoyos entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Hoyos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Hoyos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("El campo", $this->get("router")->generate("el-campo"));
        $breadcrumbs->addItem("Editar");

        return $this->render('BackendBundle:Hoyos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Hoyos entity.
    *
    * @param Hoyos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Hoyos $entity)
    {
        $form = $this->createForm(new HoyosType(), $entity, array(
            'action' => $this->generateUrl('el-campo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Hoyos entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Hoyos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Hoyos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $formRequest = $request->files->get($editForm->getName());
        $directorioBase = $this->container->getParameter('uploads.hoyos.absoluto').'imagenes/'.$entity->getDestinoImagenes().'/';

        if($formRequest['fotos'][0]){
            //Subimos las imágenes
            $upload = $this->get('uploads');
            $upload->setFiles($formRequest['fotos']);
            $upload->setDirectorio($directorioBase);

            $archivosSubidos = $upload->up();
        }

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('el-campo_show', array('id' => $id)));
        }elseif(file_exists($directorioBase) && $formRequest['fotos'][0]){
            foreach($archivosSubidos as $archivo){
                unlink($directorioBase.$archivo);
            }
        }


        return $this->render('BackendBundle:Hoyos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a Hoyos entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Hoyos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Hoyos entity.');
            }
            //Eliminamos las imágenes
            $fs = new Filesystem();
            $directorioBase = $this->container->getParameter('uploads.hoyos.absoluto').'imagenes/'.$entity->getDestinoImagenes().'/';
            $fs->remove($directorioBase);

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('el-campo'));
    }

    /**
     * Creates a form to delete a Hoyos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('el-campo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Hoyos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Hoyo entity.');
        }
        $form = $this->createDeleteForm($entity->getId());

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("El campo", $this->get("router")->generate("el-campo"));
        $breadcrumbs->addItem("Borrar");

        return $this->render('BackendBundle:Hoyos:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }

    /**
     * Elimina una imagen
     * @param $id
     * @param $nombre
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteImagenAction($id, $nombre){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Hoyos')->find($id);

        if (!$entity) {
            return new Response("false");
        }

        $directorioBase = $this->container->getParameter('uploads.hoyos.absoluto').'imagenes/'.$entity->getDestinoImagenes().'/';
        if(file_exists($directorioBase.$nombre)){
            unlink($directorioBase.$nombre);
        }

        return new Response("true");

    }

    public function imagenPrincipalAction($id, $nombre){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Hoyos')->find($id);

        if (!$entity) {
            return new Response("false");
        }
        $entity->setPrincipal($nombre);
        $em->flush();

        return new Response("true");
    }
}

