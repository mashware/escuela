<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\Instalaciones;
use Escuela\BackendBundle\Form\InstalacionesType;

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Instalaciones controller.
 *
 */
class InstalacionesController extends Controller
{

    /**
     * Lists all Instalaciones entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:Instalaciones')->findAll();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Instalaciones");
        //End Breadcrumbs

        return $this->render('BackendBundle:Instalaciones:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Instalaciones entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Instalaciones();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);

            //Subimos las imágenes
            $formRequest = $request->files->get($form->getName());
            $directorioBase = $this->container->getParameter('uploads.instalaciones.absoluto').'imagenes/'.$entity->getCarpeta().'/';
            $upload = $this->get('uploads');
            $upload->setFiles($formRequest['fotos']);
            $upload->setDirectorio($directorioBase);
            $archivosSubidos = $upload->up();

            $em->flush();

            return $this->redirect($this->generateUrl('backend_instalaciones_show', array('id' => $entity->getId())));
        }elseif(file_exists($directorioBase)){
            //Eliminamos las imágenes
            $fs = new Filesystem();
            $fs->remove($directorioBase);
        }

        return $this->render('BackendBundle:Instalaciones:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Instalaciones entity.
     *
     * @param Instalaciones $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Instalaciones $entity)
    {
        $form = $this->createForm(new InstalacionesType(), $entity, array(
            'action' => $this->generateUrl('backend_instalaciones_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Instalaciones entity.
     *
     */
    public function newAction()
    {
        $entity = new Instalaciones();
        $form   = $this->createCreateForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Instalaciones", $this->get("router")->generate("backend_instalaciones"));
        $breadcrumbs->addItem("Crear");
        //End Breadcrumbs

        return $this->render('BackendBundle:Instalaciones:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Instalaciones entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Instalaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Instalaciones entity.');
        }
        $finder = new Finder();
        $urlBaseImagenes = $this->container->getParameter('uploads.instalaciones.absoluto').'imagenes/'.$entity->getCarpeta();
        $iterator = $finder->files()->in($urlBaseImagenes);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Instalaciones", $this->get("router")->generate("backend_instalaciones"));
        $breadcrumbs->addItem("Ver");
        //End Breadcrumbs

        return $this->render('BackendBundle:Instalaciones:show.html.twig', array(
            'entity'      => $entity,
            'imagenes' => $iterator
        ));
    }

    /**
     * Displays a form to edit an existing Instalaciones entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Instalaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Instalaciones entity.');
        }

        $editForm = $this->createEditForm($entity);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Instalaciones", $this->get("router")->generate("backend_instalaciones"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Instalaciones:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Instalaciones entity.
    *
    * @param Instalaciones $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Instalaciones $entity)
    {
        $form = $this->createForm(new InstalacionesType(), $entity, array(
            'action' => $this->generateUrl('backend_instalaciones_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Instalaciones entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:Instalaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Instalaciones entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            // Subimos las imágenes
            $formRequest = $request->files->get($editForm->getName());

            if($formRequest['fotos'][0]){

                $directorioBase = $this->container->getParameter('uploads.instalaciones.absoluto').'imagenes/'.$entity->getCarpeta().'/';

                $upload = $this->get('uploads');
                $upload->setFiles($formRequest['fotos']);
                $upload->setDirectorio($directorioBase);

                $archivosSubidos = $upload->up();
            }

            $em->flush();

            return $this->redirect($this->generateUrl('backend_instalaciones_show', array('id' => $id)));
        }elseif(file_exists($directorioBase) && $formRequest['fotos'][0]){
            foreach($archivosSubidos as $archivo){
                unlink($directorioBase.$archivo);
            }
        }

        return $this->render('BackendBundle:Instalaciones:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Instalaciones entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:Instalaciones')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Instalaciones entity.');
            }

            $em->remove($entity);

            //Eliminamos las imágenes
            $fs = new Filesystem();
            $directorioBase = $this->container->getParameter('uploads.instalaciones.absoluto').'imagenes/'.$entity->getCarpeta().'/';
            $fs->remove($directorioBase);

            $em->flush();

        }

        return $this->redirect($this->generateUrl('backend_instalaciones'));
    }

    /**
     * Creates a form to delete a Instalaciones entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_instalaciones_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Instalaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Instalaciones entity.');
        }
        $form = $this->createDeleteForm($entity->getId());

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Escuela", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'escuela')));
        $breadcrumbs->addItem("Instalaciones", $this->get("router")->generate("backend_instalaciones"));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Instalaciones:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }

    /**
     * Elimina una imagen
     * @param $id
     * @param $nombre
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteImagenAction($id, $nombre){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:Instalaciones')->find($id);

        if (!$entity) {
            return new Response("false");
        }

        $directorioBase = $this->container->getParameter('uploads.instalaciones.absoluto').'imagenes/'.$entity->getCarpeta().'/';
        if(file_exists($directorioBase.$nombre)){
            unlink($directorioBase.$nombre);
        }
        return new Response("true");

    }
}
