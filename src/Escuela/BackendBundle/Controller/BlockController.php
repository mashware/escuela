<?php

namespace Escuela\BackendBundle\Controller;

use Escuela\BackendBundle\Form\BlocksType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Escuela\BackendBundle\Entity\Blocks;
use Symfony\Component\HttpFoundation\Request;

class BlockController extends Controller
{
    /**
     * Muestra un listado de los bloques
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $blocks = $em->getRepository('BackendBundle:Blocks')->findAll();
        $blocksHome = $em->getRepository('BackendBundle:Blocks')->findBlocksVisualizadosHome();

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Bloques");
        //End Breadcrumbs

        return $this->render('BackendBundle:Blocks:index.html.twig', array(
            'blocks' => $blocks,
            'blocksHome' => $blocksHome));
    }

    /**
     * Edit block
     *
     * @ParamConverter ("blocks", class="BackendBundle:Blocks")
     *
     * @param Blocks $blocks
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editAction(Blocks $blocks)
    {
        if (!$blocks) {
            throw $this->createNotFoundException('Unable to find Block entity.');
        }
        $editForm = $this->createEditBlockForm($blocks);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Bloques", $this->get("router")->generate("backend_block"));
        $breadcrumbs->addItem("Editar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Blocks:edit.html.twig', array(
            'entity' => $blocks,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Actualiza la entidad enviada
     *
     * @ParamConverter ("block", class="BackendBundle:Blocks")
     *
     * @param Request $request
     * @param Blocks $block
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function updateBlockAction(Request $request, Blocks $block)
    {
        if (!$block) {
            throw $this->createNotFoundException('No existe el bloque que intenta actualizar');
        }

        $editForm = $this->createEditBlockForm($block);
        $editForm->handleRequest($request);

        $formRequest = $request->files->get($editForm->getName());
        $imagenEnBBDD = $block->getImg();

        if($formRequest['foto']){
            $directorioBase = $this->container->getParameter('uploads.blocks.absoluto').'imagenes/';

            //Subimos las imágenes
            $upload = $this->get('uploads');
            $upload->setFiles($formRequest['foto']);
            $upload->setDirectorio($directorioBase);

            $archivosSubidos = $upload->up();

            $block->setImg($archivosSubidos->first());
        }
        $directorioBase = $this->container->getParameter('uploads.blocks.absoluto').'imagenes/';

        if ($editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            if(file_exists($directorioBase.$imagenEnBBDD) && $formRequest['foto']){
                unlink($directorioBase.$imagenEnBBDD);
            }
            return $this->redirect($this->generateUrl('backend_block'));
        }else{
            if(file_exists($directorioBase.$block->getImg())){
                unlink($directorioBase.$block->getImg());
            }
        }

        return $this->render('BackendBundle:Blocks:edit.html.twig', array(
            'entity' => $block,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Crea el formulario de añadir un block
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newBlockAction(){
        $block = new Blocks();

        $form = $this->createNewBlockForm($block);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Bloques", $this->get("router")->generate("backend_block"));
        $breadcrumbs->addItem("Nuevo");
        //End Breadcrumbs

        return $this->render('BackendBundle:Blocks:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Crea el formulario de edicion de la entidad enviada
     *
     * @param $entity
     * @return \Symfony\Component\Form\Form
     */
    private function createEditBlockForm(Blocks $entity)
    {
        $editForm = $this->createForm(new BlocksType(), $entity, array(
            'action' => $this->generateUrl('backend_block_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        $editForm->add('submit', 'submit', array('label' => 'Actualizar'));

        return $editForm;
    }

    /**
     * Crea el formulario de insercción de la entidad enviada
     *
     * @param $entity
     * @return \Symfony\Component\Form\Form
     */
    private function createNewBlockForm(Blocks $entity)
    {
        $form = $this->createForm(new BlocksType(), $entity, array(
            'action' => $this->generateUrl('backend_block_create'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Añadir'));

        return $form;
    }

    /**
     * Crea un bloque nuevo
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createBlockAction(Request $request){
        $block = new Blocks();

        $form = $this->createNewBlockForm($block);
        $form->handleRequest($request);

        //Subimos las imágenes
        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.blocks.absoluto').'imagenes/';

        $upload = $this->get('uploads');
        //Foto pequeña
        $upload->setFiles($formRequest['foto']);
        $upload->setDirectorio($directorioBase);
        $archivosSubidos = $upload->up();

        $block->setImg($archivosSubidos->first());

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();

            $em->persist($block);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_block'));
        }elseif($block->getImg()){
           if(file_exists($directorioBase.$block->getImg())){
               unlink($directorioBase.$block->getImg());
           }
        }

        return $this->render('BackendBundle:Blocks:new.html.twig', array(
            'entity' => $block,
            'form' => $form->createView()
        ));
    }

    /**
     * Crea el formulario de eliminación
     *
     * @param Blocks $block
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm(Blocks $block){
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_block_delete', array('id' => $block->getId())))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();

    }

    /**
     * Muestra la el formulario de eliminación de block
     *
     * @ParamConverter ("blocks", class="BackendBundle:Blocks")
     *
     * @param Blocks $block
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction(Blocks $block){
        if(!$block){
            throw $this->createNotFoundException('El elemento que desea borrar no existe');
        }
        $form = $this->createDeleteForm($block);

        //Breadcrumbs
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("backend_dashboard"));
        $breadcrumbs->addItem("Página Web", $this->get("router")->generate("backend_dashboard_slug", array('slug' => 'pagina-web')));
        $breadcrumbs->addItem("Bloques", $this->get("router")->generate("backend_block"));
        $breadcrumbs->addItem("Borrar");
        //End Breadcrumbs

        return $this->render('BackendBundle:Blocks:remove.html.twig', array(
            'entity' => $block,
            'delete_form' => $form->createView()
        ));
    }

    /**
     * Elimina el block enviado
     *
     * @ParamConverter ("blocks", class="BackendBundle:Blocks")
     *
     * @param Request $request
     * @param $block
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteBlockAction(Request $request, Blocks $block){
        if(!$block){
            throw $this->createNotFoundException('El elemento que intentas eliminar no existe');
        }
        $form = $this->createDeleteForm($block);
        $form->handleRequest($request);

        $directorioBase = $this->container->getParameter('uploads.blocks.absoluto').'imagenes/';
        if($form->isValid()){
            if(file_exists($directorioBase.$block->getImg())){
                unlink($directorioBase.$block->getImg());
            }
            $em = $this->getDoctrine()->getManager();

            $em->remove($block);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_block'));

    }
}
