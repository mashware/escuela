<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class DashboardController
 * @package Escuela\BackendBundle\Controller
 */
class DashboardController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('BackendBundle:Dashboard:index.html.twig');
    }

    public function loginAction(Request $request){
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render('BackendBundle:Dashboard:login.html.twig', array(
            // last username entered by the user
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error'         => $error,
        ));
    }

    public function partnersAction()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $partnerCategorias = $em->getRepository('BackendBundle:PartnerCategorias')->findAll();

        return $this->render('BackendBundle:Dashboard:partner.html.twig', array('categorias' => $partnerCategorias));
    }

    public function productosAction()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $categorias = $em->getRepository('BackendBundle:TiendaCategoria')->findAll();

        return $this->render('BackendBundle:Dashboard:productos.html.twig', array('categorias' => $categorias));
    }

    public function slugAction($slug)
    {
        return $this->render('BackendBundle:Dashboard:'.$slug.'.html.twig');
    }
}
