<?php

namespace Escuela\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Escuela\BackendBundle\Entity\TiendaProducto;
use Escuela\BackendBundle\Form\TiendaProductoType;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

/**
 * TiendaProducto controller.
 *
 */
class TiendaProductoController extends Controller
{

    /**
     * Lists all TiendaProducto entities.
     *
     * @param $slugCategoria
     * @return Response
     */
    public function indexAction($slugCategoria)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BackendBundle:TiendaProducto')->findByCategoriasAll($slugCategoria);

        return $this->render('BackendBundle:TiendaProducto:index.html.twig', array(
            'entities' => $entities,
            'slugCategoria' => $slugCategoria
        ));
    }

    /**
     * Creates a new TiendaProducto entity.
     *
     * @param Request $request
     * @param $slugCategoria
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request, $slugCategoria)
    {
        $em = $this->getDoctrine()->getManager();

        $categoria = $em->getRepository('BackendBundle:TiendaCategoria')->findOneBy(array('slug' => $slugCategoria));

        if (!$categoria) {
            throw $this->createNotFoundException('Unable to find Categoria entity.');
        }

        $entity = new TiendaProducto();
        $entity->setCategoria($categoria);

        $form = $this->createCreateForm($entity, $slugCategoria);
        $form->handleRequest($request);

        $entity->setImagenes(uniqid());

        // Subimos imagenes
        $formRequest = $request->files->get($form->getName());
        $directorioBase = $this->container->getParameter('uploads.productos.absoluto').'imagenes/';

        $upload = $this->get('uploads');
        // Imagen principal
        if($formRequest['principal']){
            $upload->setFiles($formRequest['principal']);
            $upload->setDirectorio($directorioBase);
            $archivosSubidos = $upload->up();
            $entity->setImagen($archivosSubidos->first());
        }
        // Resto de imágenes
        if($formRequest['fotos'][0]){
            $upload->setDirectorio($directorioBase.$entity->getImagenes().'/');
            $upload->setFiles($formRequest['fotos']);
            $archivosSubidos = $upload->up();
        }

        if ($form->isValid()) {

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_tienda_producto_show', array('slugCategoria' => $slugCategoria, 'id' => $entity->getId())));
        }
        if(file_exists($directorioBase.$entity->getImagenes().'/')){
            //Eliminamos las imágenes
            $fs = new Filesystem();
            $fs->remove($directorioBase.$entity->getImagenes().'/');
        }
        if(file_exists($directorioBase.$entity->getImagen()) && $entity->getImagen()){
            unlink($directorioBase.$entity->getImagen());
        }
        return $this->render('BackendBundle:TiendaProducto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TiendaProducto entity.
     *
     * @param TiendaProducto $entity
     * @param $slugCategoria
     * @return \Symfony\Component\Form\Form
     */
    private function createCreateForm(TiendaProducto $entity, $slugCategoria)
    {
        $form = $this->createForm(new TiendaProductoType(), $entity, array(
            'action' => $this->generateUrl('backend_tienda_producto_create', array('slugCategoria' => $slugCategoria)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TiendaProducto entity.
     *
     * @param $slugCategoria
     * @return Response
     */
    public function newAction($slugCategoria)
    {
        $entity = new TiendaProducto();
        $form   = $this->createCreateForm($entity, $slugCategoria);

        return $this->render('BackendBundle:TiendaProducto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TiendaProducto entity.
     *
     * @param $slugCategoria
     * @param $id
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function showAction($slugCategoria, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:TiendaProducto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TiendaProducto entity.');
        }

        $finder = new Finder();
        $urlBaseImagenes = $this->container->getParameter('uploads.productos.absoluto').'imagenes/'.$entity->getImagenes();
        $iterator = $finder->files()->in($urlBaseImagenes);

        return $this->render('BackendBundle:TiendaProducto:show.html.twig', array(
            'entity'    => $entity,
            'imagenes'  => $iterator,
            'slugCategoria' => $slugCategoria
        ));
    }

    /**
     * Displays a form to edit an existing TiendaProducto entity.
     *
     * @param $slugCategoria
     * @param $id
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editAction($slugCategoria, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:TiendaProducto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TiendaProducto entity.');
        }

        $editForm = $this->createEditForm($entity, $slugCategoria);

        return $this->render('BackendBundle:TiendaProducto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView()
        ));
    }

    /**
     * Creates a form to edit a TiendaProducto entity.
     *
     * @param TiendaProducto $entity
     * @param $slugCategoria
     * @return \Symfony\Component\Form\Form
     */
    private function createEditForm(TiendaProducto $entity, $slugCategoria)
    {
        $form = $this->createForm(new TiendaProductoType(), $entity, array(
            'action' => $this->generateUrl('backend_tienda_producto_update', array('slugCategoria' => $slugCategoria,'id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing TiendaProducto entity.
     *
     * @param Request $request
     * @param $id
     * @param $slugCategoria
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function updateAction(Request $request, $id, $slugCategoria)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BackendBundle:TiendaProducto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TiendaProducto entity.');
        }

        $editForm = $this->createEditForm($entity, $slugCategoria);
        $editForm->handleRequest($request);

        // Subimos imagenes
        $formRequest = $request->files->get($editForm->getName());
        $directorioBase = $this->container->getParameter('uploads.productos.absoluto').'imagenes/';

        $upload = $this->get('uploads');
        // Imagen principal
        if($formRequest['principal']){
            $upload->setFiles($formRequest['principal']);
            $upload->setDirectorio($directorioBase);
            $archivosSubidos = $upload->up();
            $imgagenBBDDPrincipal = $entity->getImagen();
            $entity->setImagen($archivosSubidos->first());
        }
        // Restos de imágenes
        if($formRequest['fotos'][0]){
            $upload->setDirectorio($directorioBase.$entity->getImagenes().'/');
            $upload->setFiles($formRequest['fotos']);
            $fotos = $upload->up();
        }

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('backend_tienda_producto_show', array('slugCategoria' => $slugCategoria, 'id' => $id)));
        }

        if(file_exists($directorioBase.$entity->getImagenes().'/')){
            //Eliminamos las imágenes
            foreach($fotos as $foto){
                unlink($directorioBase.$entity->getImagenes().'/'.$foto);
            }
        }
        if(file_exists($directorioBase.$entity->getImagen())){
            if($imgagenBBDDPrincipal != $entity->getImagen()){
                unlink($directorioBase.$entity->getImagen());
            }
        }

        return $this->render('BackendBundle:TiendaProducto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
     * Deletes a TiendaProducto entity.
     *
     * @param Request $request
     * @param $id
     * @param $slugCategoria
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction(Request $request, $id, $slugCategoria)
    {
        $form = $this->createDeleteForm($id, $slugCategoria);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BackendBundle:TiendaProducto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TiendaProducto entity.');
            }

            //Eliminamos las imágenes
            $fs = new Filesystem();
            $directorioBase = $this->container->getParameter('uploads.productos.absoluto').'imagenes/';
            $fs->remove($directorioBase.$entity->getImagenes().'/');
            if(file_exists($directorioBase.$entity->getImagen())){
                unlink($directorioBase.$entity->getImagen());
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_tienda_producto', array('slugCategoria' => $slugCategoria)));
    }

    /**
     * Creates a form to delete a TiendaProducto entity by id.
     *
     * @param $id
     * @param $slugCategoria
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm($id, $slugCategoria)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_tienda_producto_delete', array('slugCategoria' => $slugCategoria, 'id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Muestra el formulario de eliminación
     *
     * @param $id
     * @param $slugCategoria
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteFormAction($id, $slugCategoria){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:TiendaProducto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Producto entity.');
        }
        $form = $this->createDeleteForm($entity->getId(), $slugCategoria);

        return $this->render('BackendBundle:TiendaProducto:remove.html.twig', array(
            'entity' => $entity,
            'delete_form' => $form->createView()
        ));
    }

    /**
     * Elimina una imagen
     * @param $id
     * @param $nombre
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteImagenAction($id, $nombre){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BackendBundle:TiendaProducto')->find($id);

        if (!$entity) {
            return new Response("false");
        }

        $directorioBase = $this->container->getParameter('uploads.productos.absoluto').'imagenes/'.$entity->getImagenes().'/';
        if(file_exists($directorioBase.$nombre)){
            unlink($directorioBase.$nombre);
        }

        return new Response("true");

    }

}
