<?php

namespace Escuela\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Partner
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Escuela\BackendBundle\Entity\PartnerRepository")
 * @Gedmo\TranslationEntity(class="Escuela\BackendBundle\Entity\PartnerTranslation")
 */
class Partner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="provincia", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $provincia;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="descripcion", type="string", length=255)
     * @Assert\NotBlank()
     *
     */
    private $descripcion;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="texto", type="text")
     * @Assert\NotBlank()
     *
     */
    private $texto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="oro", type="boolean")
     */
    private $oro = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     * @Assert\NotBlank()
     *
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="contacto", type="string", length=255)
     * @Assert\NotBlank()
     *
     */
    private $contacto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado = false;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="imagenes", type="string", length=255)
     *
     */
    private $imagenes;

    /**
     * @var string
     *
     * @ORM\Column(name="imagenPrincipal", type="string", length=255)
     *
     */
    private $imagenPrincipal;

    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="destacado", type="boolean")
     */
    private $destacado = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordenDestacado", type="integer")
     */
    private $ordenDestacado = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="grande", type="boolean")
     */
    private $grande = false;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Escuela\BackendBundle\Entity\PartnerCategorias")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    private $categoria;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    /**
     * @ORM\OneToMany(targetEntity="PartnerTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    private $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function addTranslation(PartnerTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Partner
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set provincia
     *
     * @param string $provincia
     * @return Partner
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return string 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Partner
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return Partner
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set oro
     *
     * @param boolean $oro
     * @return Partner
     */
    public function setOro($oro)
    {
        $this->oro = $oro;

        return $this;
    }

    /**
     * Get oro
     *
     * @return boolean 
     */
    public function getOro()
    {
        return $this->oro;
    }

    /**
     * Set telefono
     *
     * @param integer $telefono
     * @return Partner
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return integer 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set contacto
     *
     * @param string $contacto
     * @return Partner
     */
    public function setContacto($contacto)
    {
        $this->contacto = $contacto;

        return $this;
    }

    /**
     * Get contacto
     *
     * @return string 
     */
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Partner
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Partner
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set imagenes
     *
     * @param string $imagenes
     * @return Partner
     */
    public function setImagenes($imagenes)
    {
        $this->imagenes = $imagenes;

        return $this;
    }

    /**
     * Get imagenes
     *
     * @return string 
     */
    public function getImagenes()
    {
        return $this->imagenes;
    }

    /**
     * Set imagenPrincipal
     *
     * @param string $imagenPrincipal
     * @return Partner
     */
    public function setImagenPrincipal($imagenPrincipal)
    {
        $this->imagenPrincipal = $imagenPrincipal;

        return $this;
    }

    /**
     * Get imagenPrincipal
     *
     * @return string 
     */
    public function getImagenPrincipal()
    {
        return $this->imagenPrincipal;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Partner
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set destacado
     *
     * @param boolean $destacado
     * @return Partner
     */
    public function setDestacado($destacado)
    {
        $this->destacado = $destacado;

        return $this;
    }

    /**
     * Get destacado
     *
     * @return boolean 
     */
    public function getDestacado()
    {
        return $this->destacado;
    }

    /**
     * Set ordenDestacado
     *
     * @param integer $ordenDestacado
     * @return Partner
     */
    public function setOrdenDestacado($ordenDestacado)
    {
        $this->ordenDestacado = $ordenDestacado;

        return $this;
    }

    /**
     * Get ordenDestacado
     *
     * @return integer 
     */
    public function getOrdenDestacado()
    {
        return $this->ordenDestacado;
    }

    /**
     * Set categoria
     * @param PartnerCategorias $categoria
     * @return $this
     */
    public function setCategoria(\Escuela\BackendBundle\Entity\PartnerCategorias $categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param int $grande
     */
    public function setGrande($grande)
    {
        $this->grande = $grande;
    }

    /**
     * @return int
     */
    public function getGrande()
    {
        return $this->grande;
    }

}
