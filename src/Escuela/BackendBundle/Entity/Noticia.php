<?php

namespace Escuela\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Noticia
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Escuela\BackendBundle\Entity\NoticiaRepository")
 * @Gedmo\TranslationEntity(class="Escuela\BackendBundle\Entity\NoticiaTranslation")
 */
class Noticia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="titulo", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $titulo;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="introduccion", type="text")
     * @Assert\NotBlank()
     */
    private $introduccion;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="texto", type="text")
     * @Assert\NotBlank()
     */
    private $texto;

    /**
     * @var string
     *
     * @ORM\Column(name="fuente", type="string", length=255)
     * @Assert\Url()
     */
    private $fuente;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     * @Assert\Type(type="bool")
     */
    private $estado = false;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="escuela", type="boolean")
     */
    private $escuela = true;

    /**
     * @var string
     *
     * @ORM\Column(name="circuito", type="boolean")
     */
    private $circuito = true;

    /**
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    /**
     * @ORM\OneToMany(targetEntity="NoticiaTranslation", mappedBy="object", cascade={"persist", "remove"})
     */
    private $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function addTranslation(NoticiaTranslation $t)
    {
        if (!$this->translations->contains($t)) {
            $this->translations[] = $t;
            $t->setObject($this);
        }
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Noticia
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set introduccion
     *
     * @param string $introduccion
     * @return Noticia
     */
    public function setIntroduccion($introduccion)
    {
        $this->introduccion = $introduccion;

        return $this;
    }

    /**
     * Get introduccion
     *
     * @return string 
     */
    public function getIntroduccion()
    {
        return $this->introduccion;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return Noticia
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Noticia
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set fuente
     *
     * @param string $fuente
     * @return Noticia
     */
    public function setFuente($fuente)
    {
        $this->fuente = $fuente;

        return $this;
    }

    /**
     * @return string
     */
    public function getFuente()
    {
        return $this->fuente;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Noticia
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param $escuea
     * @return $this
     */
    public function setEscuela($escuea)
    {
        $this->escuela = $escuea;

        return $this;
    }

    /**
     * @return string
     */
    public function getEscuela()
    {
        return $this->escuela;
    }
    /**
     * @param string $circuito
     */
    public function setCircuito($circuito)
    {
        $this->circuito = $circuito;
    }

    /**
     * @return string
     */
    public function getCircuito()
    {
        return $this->circuito;
    }

    public function getSlug()
    {
        return $this->slug;
    }
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return  $this;
    }

}
