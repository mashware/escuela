<?php

namespace Escuela\BackendBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * NoticiaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NoticiaRepository extends EntityRepository
{
    /**
     * @param $slug
     * @return mixed
     */
    public function findSlug($slug){
        $em = $this->getEntityManager();

        $dql = 'SELECT COUNT(n.id)
            FROM BackendBundle:Noticia n
            WHERE n.slug = :slug';
        $query = $em->createQuery($dql);
        $query->setParameter('slug', $slug);

        return $query->getSingleScalarResult();
    }

    /**
     * @param $slug
     * @param $locale
     * @return mixed
     */
    public function findSlugByIdioma($slug, $locale){
        $em = $this->getEntityManager();

        $dql = 'SELECT COUNT(nt.id)
            FROM BackendBundle:NoticiaTranslation nt
            WHERE nt.field = :field
            AND nt.content = :content
            AND nt.locale = :locale';
        $query = $em->createQuery($dql);
        $query->setParameter('field', 'slug');
        $query->setParameter('locale', $locale);
        $query->setParameter('content', $slug);

        return $query->getSingleScalarResult();
    }
    public function findBySlug($slug){
        $em = $this->getEntityManager();

        $dql = 'SELECT n
            FROM BackendBundle:Noticia n
            WHERE n.slug = :slug
            AND n.estado = :estado';
        $query = $em->createQuery($dql);
        $query->setParameter('slug', $slug);
        $query->setParameter('estado', true);


        return $query->getSingleResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    /**
     * @param $object
     * @return mixed
     */
    public function findTranlateSlugByObject($object){
        $em = $this->getEntityManager();

        $dql = 'SELECT nt
            FROM BackendBundle:NoticiaTranslation nt
            WHERE nt.object = :object
            AND nt.field = :field';
        $query = $em->createQuery($dql);
        $query->setParameter('object', $object);
        $query->setParameter('field', 'slug');

        return $query->getOneOrNullResult();
    }

    /**
     * @param $limite
     * @return array
     */
    public function findNoticiasEscuela($limite){
        $em = $this->getEntityManager();

        $dql = 'SELECT n
            FROM BackendBundle:Noticia n
            WHERE n.estado = :estado
            AND n.escuela = :escuela
            ORDER BY n.created DESC';
        $query = $em->createQuery($dql);
        $query->setParameter('estado', true);
        $query->setParameter('escuela', true);

        $query->setMaxResults($limite);

        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }

    public function findAllQuery(){
        $em = $this->getEntityManager();
        $dql   = "SELECT n FROM BackendBundle:Noticia n";
        $query = $em->createQuery($dql);

        return $query;
    }

    public function findNoticiasCircuito($page, $limite){
        $em = $this->getEntityManager();

        $dql = 'SELECT n
            FROM BackendBundle:Noticia n
            WHERE n.estado = :estado
            AND n.circuito = :circuito
            ORDER BY n.id DESC';
        $query = $em->createQuery($dql);
        $query->setParameter('estado', true);
        $query->setParameter('circuito', true);

        $query->setFirstResult($page * $limite);
        $query->setMaxResults($limite);

        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}
