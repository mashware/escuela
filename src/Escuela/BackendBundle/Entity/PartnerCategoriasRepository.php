<?php

namespace Escuela\BackendBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PartnerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PartnerCategoriasRepository extends EntityRepository
{
    /**
     * @param $slug
     * @return mixed
     */
    public function findSlug($slug){
        $em = $this->getEntityManager();

        $dql = 'SELECT COUNT(p.id)
            FROM BackendBundle:Partner p
            WHERE p.slug = :slug';
        $query = $em->createQuery($dql);
        $query->setParameter('slug', $slug);

        return $query->getSingleScalarResult();
    }

    /**
     * @param $slug
     * @param $locale
     * @return mixed
     */
    public function findSlugByIdioma($slug, $locale){
        $em = $this->getEntityManager();

        $dql = 'SELECT COUNT(pt.id)
            FROM BackendBundle:PartnerCategoriasTranslation pt
            WHERE pt.field = :field
            AND pt.content = :content
            AND pt.locale = :locale';
        $query = $em->createQuery($dql);
        $query->setParameter('field', 'slug');
        $query->setParameter('locale', $locale);
        $query->setParameter('content', $slug);

        return $query->getSingleScalarResult();
    }
    public function findBySlug($slug, $hidrate = \Doctrine\ORM\Query::HYDRATE_ARRAY){
        $em = $this->getEntityManager();

        $dql = 'SELECT p
            FROM BackendBundle:PartnerCategorias p
            WHERE p.slug = :slug';
        $query = $em->createQuery($dql);
        $query->setParameter('slug', $slug);


        return $query->getOneOrNullResult($hidrate);
    }

    /**
     * @param $object
     * @return mixed
     */
    public function findTranlateSlugByObject($object){
        $em = $this->getEntityManager();

        $dql = 'SELECT pt
            FROM BackendBundle:PartnerCategoriasTranslation pt
            WHERE pt.object = :object
            AND pt.field = :field';
        $query = $em->createQuery($dql);
        $query->setParameter('object', $object);
        $query->setParameter('field', 'slug');

        return $query->getOneOrNullResult();
    }
}
