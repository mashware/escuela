<?php
/**
 * Created by PhpStorm.
 * User: Alberto
 * Date: 20/05/14
 * Time: 20:42
 */

namespace Escuela\BackendBundle\Utiles;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Uploads
 * @package Escuela\BackendBundle\Utiles
 */
class Uploads
{
    private $files;

    private $archivosSubidos;

    private $directorio;

    public function __construct()
    {
        $this->files = new ArrayCollection();
        $this->archivosSubidos = new ArrayCollection();
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
        $this->archivosSubidos->clear();
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param mixed $directorio
     */
    public function setDirectorio($directorio)
    {
        $this->directorio = $directorio;
    }

    /**
     * @return mixed
     */
    public function getDirectorio()
    {
        return $this->$directorio;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getArchivosSubidos()
    {
        return $this->archivosSubidos;
    }

    /**
     * Sube los archivos
     *
     * @param null $nombre Only in simple uploads not multiplet uploads
     * @return ArrayCollection
     */
    public function up($nombre = null)
    {
        //Si son varios
        if (is_array($this->files)){
            foreach($this->files AS $file){
                $nombre = uniqid().'.'.$file->getClientOriginalExtension();

                $file->move( $this->directorio, $nombre);
                $this->archivosSubidos->add($nombre);
            }

            return $this->archivosSubidos;
        }else if (null !== $this->files){
            //Si es solo una foto
            if($nombre == null){
                $nombre = uniqid().'.'.$this->files->getClientOriginalExtension();
            }
            $this->files->move( $this->directorio, $nombre);
            $this->archivosSubidos->add($nombre);

            return $this->archivosSubidos;
        }else{
            //Si no es ninguna de las dos salgo
            return $this->archivosSubidos;
        }
    }
    public function move(){
        if (!is_array($this->files)){
            return;
        }
        foreach($this->files AS $file){
            $file->move($this->directorio, $file->getRelativePathname());
        }
    }
}