<?php
    namespace Escuela\BackendBundle\DataFixtures\ORM;

    use Doctrine\Common\DataFixtures\AbstractFixture;
    use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
    use Doctrine\Common\Persistence\ObjectManager;
    use Escuela\BackendBundle\Entity\BlockSections;

    class BlockSectionsFix extends AbstractFixture implements OrderedFixtureInterface
    {
        public function getOrder(){
            return 4;
        }
        public function load(ObjectManager $manager)
        {
            $secciones = array('home');

            foreach ($secciones as $seccion) {
                $entidad = new BlockSections();

                $entidad->setName($seccion);

                $manager->persist($entidad);
            }

            $manager->flush();
        }
    }