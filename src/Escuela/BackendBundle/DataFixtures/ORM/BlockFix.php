<?php
    namespace Escuela\BackendBundle\DataFixtures\ORM;

    use Doctrine\Common\DataFixtures\AbstractFixture;
    use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
    use Doctrine\Common\Persistence\ObjectManager;
    use Escuela\BackendBundle\Entity\Blocks;

    class BlockFix extends AbstractFixture implements OrderedFixtureInterface
    {
        public function getOrder(){
            return 5;
        }
        public function load(ObjectManager $manager)
        {
            $blockSections = $manager->getRepository('BackendBundle:BlockSections')->findAll();

            foreach ($blockSections as $section) {
                $total = 5;
                for ($i=0; $i<=$total; $i++) {
                    $entidad = new Blocks();

                    $entidad->setTitle($this->getTitle());
                    $entidad->setText($this->getTexto());
                    $entidad->setBlockSections($section);
                    $entidad->setImg($this->getImagen());
                    $entidad->setPosition($i);
                    $entidad->setState(rand(0,1));
                    $entidad->setUrl($this->getUrl());
                    $manager->persist($entidad);
                }
            }
            $manager->flush();
        }
        /**
         * Generador aleatorio de nombres de ofertas.
         *
         * @return string Nombre/título aletorio generado para la oferta.
         */
        private function getTitle()
        {
            $palabras = array_flip(array(
                'Lorem', 'Ipsum', 'Sitamet', 'Et', 'At', 'Sed', 'Aut', 'Vel', 'Ut',
                'Dum', 'Tincidunt', 'Facilisis', 'Nulla', 'Scelerisque', 'Blandit',
                'Ligula', 'Eget', 'Drerit', 'Malesuada', 'Enimsit', 'Libero',
                'Penatibus', 'Imperdiet', 'Pendisse', 'Vulputae', 'Natoque',
                'Aliquam', 'Dapibus', 'Lacinia'
            ));

            $numeroPalabras = rand(4, 8);

            return implode(' ', array_rand($palabras, $numeroPalabras));
        }
        /**
         * Generador aleatorio Texto.
         *
         * @return string Texto aletoria generado.
         */
        private function getTexto()
        {
            $frases = array_flip(array(
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                'Mauris ultricies nunc nec sapien tincidunt facilisis.',
                'Nulla scelerisque blandit ligula eget hendrerit.',
                'Sed malesuada, enim sit amet ultricies semper, elit leo lacinia massa, in tempus nisl ipsum quis libero.',
                'Aliquam molestie neque non augue molestie bibendum.',
                'Pellentesque ultricies erat ac lorem pharetra vulputate.',
                'Donec dapibus blandit odio, in auctor turpis commodo ut.',
                'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
                'Nam rhoncus lorem sed libero hendrerit accumsan.',
                'Maecenas non erat eu justo rutrum condimentum.',
                'Suspendisse leo tortor, tempus in lacinia sit amet, varius eu urna.',
                'Phasellus eu leo tellus, et accumsan libero.',
                'Pellentesque fringilla ipsum nec justo tempus elementum.',
                'Aliquam dapibus metus aliquam ante lacinia blandit.',
                'Donec ornare lacus vitae dolor imperdiet vitae ultricies nibh congue.',
            ));

            $numeroFrases = rand(4, 7);

            return implode("\n", array_rand($frases, $numeroFrases));
        }

        /**
         * Generador de imagen
         * @return mixed
         */
        private function getImagen(){
            $imagen = array('sky', 'vine', 'lava', 'gray', 'industrial', 'social');

            return 'holder.js/226x158/'.$imagen[array_rand($imagen, 1)];
        }
        /**
         * Generador url
         * @return mixed
         */
        private function getUrl(){
            $url = array('www.google.es', 'www.symfony.es', 'www.symfony.com', 'wwww.mashware.es', 'www.lowgolf.es', 'www.facebook.es');

            return 'http://'.$url[array_rand($url, 1)];
        }
    }