<?php
namespace Escuela\BackendBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Escuela\BackendBundle\Entity\Blocks;
use Escuela\BackendBundle\Entity\BlocksTranslation;
use Escuela\BackendBundle\Utiles\Util;

/**
 * Class BlocksSlugListener
 * @package Escuela\BackendBundle\Event\EventListener
 */
class BlocksSlugListener {
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Blocks) {
            $noExiste = 0;
            $slug = "";
            $i = 0;

            do{
                $slug = Util::getSlug($entity->getTitle(), '-', $i);
                $noExiste = $entityManager->getRepository('BackendBundle:Blocks')->findSlug($slug);
                $i++;
            }while($noExiste);
            $entity->setSlug($slug);
        }else if ($entity instanceof BlocksTranslation) {
            if($entity->getField() == 'title'){
                $noExiste = 0;
                $slug = "";
                $i = 0;
                do{
                    $slug = Util::getSlug($entity->getContent(), '-', $i);
                    $noExiste = $entityManager->getRepository('BackendBundle:Blocks')->findSlugByIdioma($slug, $entity->getLocale());
                    $i++;
                }while($noExiste);
                $block = $entity->getObject();
                $slugTranslation = new BlocksTranslation($entity->getLocale(), 'slug', $slug);
                $block->addTranslation($slugTranslation);
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Blocks) {
            $noExiste = 0;
            $slug = "";
            $i = 0;

            do{
                $slug = Util::getSlug($entity->getTitle(), '-', $i);
                if ($slug != $entity->getSlug()){
                    $noExiste = $entityManager->getRepository('BackendBundle:Blocks')->findSlug($slug);
                    $i++;
                }else{
                    $noExiste = false;
                }
            }while($noExiste);
            $entity->setSlug($slug);
        }else if($entity instanceof BlocksTranslation) {
            if($entity->getField() == 'title'){
                $noExiste = 0;
                $slug = "";
                $i = 0;
                do{
                    $slug = Util::getSlug($entity->getContent(), '-', $i);
                    $noExiste = $entityManager->getRepository('BackendBundle:Blocks')->findSlugByIdioma($slug, $entity->getLocale());
                    $i++;
                }while($noExiste);
                $slugTranslation = $entityManager->getRepository('BackendBundle:Blocks')->findTranlateSlugByObject($entity->getObject()->getId());
                $block = $entity->getObject();
                //Si existe y es distinto actualizamos, sino pues añadimos (hacemos esta segunda comprobación en el if por que sino entra en un bucle infinito al guardar el mismo tipo de entidad)
                if($slugTranslation && $slugTranslation->getContent() != $slug){
                    $slugTranslation->setContent($slug);
                    $entityManager->flush();
                }elseif(!$slugTranslation){
                    $slugTranslation = new BlocksTranslation($entity->getLocale(), 'slug', $slug);

                    $block->addTranslation($slugTranslation);
                    $entityManager->flush();
                }
            }
        }
    }
}