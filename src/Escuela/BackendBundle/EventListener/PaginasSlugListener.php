<?php
namespace Escuela\BackendBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Escuela\BackendBundle\Entity\Paginas;
use Escuela\BackendBundle\Entity\PaginasTranslation;
use Escuela\BackendBundle\Utiles\Util;

/**
 * Class PaginasSlugListener
 * @package Escuela\BackendBundle\Event\EventListener
 */
class PaginasSlugListener {
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Paginas) {
            $noExiste = 0;
            $slug = "";
            $i = 0;

            do{
                $slug = Util::getSlug($entity->getTitulo(), '-', $i);
                $noExiste = $entityManager->getRepository('BackendBundle:Paginas')->findSlug($slug);
                $i++;
            }while($noExiste);
            $entity->setSlug($slug);
        }else if ($entity instanceof PaginasTranslation) {
            if($entity->getField() == 'titulo'){
                $noExiste = 0;
                $slug = "";
                $i = 0;
                do{
                    $slug = Util::getSlug($entity->getContent(), '-', $i);
                    $noExiste = $entityManager->getRepository('BackendBundle:Paginas')->findSlugByIdioma($slug, $entity->getLocale());
                    $i++;
                }while($noExiste);
                $pagina = $entity->getObject();
                $slugTranslation = new PaginasTranslation($entity->getLocale(), 'slug', $slug);
                $pagina->addTranslation($slugTranslation);
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Paginas) {
            $noExiste = 0;
            $slug = "";
            $i = 0;

            do{
                $slug = Util::getSlug($entity->getTitulo(), '-', $i);
                if ($slug != $entity->getSlug()){
                    $noExiste = $entityManager->getRepository('BackendBundle:Paginas')->findSlug($slug);
                    $i++;
                }else{
                    $noExiste = false;
                }

            }while($noExiste);


            $entity->setSlug($slug);
        }else if ($entity instanceof PaginasTranslation) {
            if($entity->getField() == 'titulo'){
                $noExiste = 0;
                $slug = "";
                $i = 0;
                do{
                    $slug = Util::getSlug($entity->getContent(), '-', $i);
                    $noExiste = $entityManager->getRepository('BackendBundle:Paginas')->findSlugByIdioma($slug, $entity->getLocale());
                    $i++;
                }while($noExiste);
                $paginas = $entity->getObject();
                $slugTranslation = $entityManager->getRepository('BackendBundle:Paginas')->findTranlateSlugByObject($paginas->getId());

                //Si existe y es distinto actualizamos, sino pues añadimos (hacemos esta segunda comprobación en el if por que sino entra en un bucle infinito al guardar el mismo tipo de entidad)
                if($slugTranslation && $slugTranslation->getContent() != $slug){
                    $slugTranslation->setContent($slug);

                    $entityManager->flush();
                }elseif(!$slugTranslation){
                    $slugTranslation = new PaginasTranslation($entity->getLocale(), 'slug', $slug);
                    $paginas->addTranslation($slugTranslation);

                    $entityManager->flush();
                }
            }
        }
    }
}