<?php
namespace Escuela\BackendBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Escuela\BackendBundle\Entity\Noticia;
use Escuela\BackendBundle\Entity\NoticiaTranslation;
use Escuela\BackendBundle\Utiles\Util;

/**
 * Class NoticiaSlugListener
 * @package Escuela\BackendBundle\Event\EventListener
 */
class NoticiaSlugListener {
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Noticia) {
            $noExiste = 0;
            $slug = "";
            $i = 0;

            do{
                $slug = Util::getSlug($entity->getTitulo(), '-', $i);
                $noExiste = $entityManager->getRepository('BackendBundle:Noticia')->findSlug($slug);
                $i++;
            }while($noExiste);
            $entity->setSlug($slug);
        }else if ($entity instanceof NoticiaTranslation) {
            if($entity->getField() == 'titulo'){
                $noExiste = 0;
                $slug = "";
                $i = 0;
                do{
                    $slug = Util::getSlug($entity->getContent(), '-', $i);
                    $noExiste = $entityManager->getRepository('BackendBundle:Noticia')->findSlugByIdioma($slug, $entity->getLocale());
                    $i++;
                }while($noExiste);
                $block = $entity->getObject();
                $slugTranslation = new NoticiaTranslation($entity->getLocale(), 'slug', $slug);
                $block->addTranslation($slugTranslation);
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();
        if ($entity instanceof Noticia) {
            $noExiste = 0;
            $slug = "";
            $i = 0;

            do{
                $slug = Util::getSlug($entity->getTitulo(), '-', $i);
                if ($slug != $entity->getSlug()){
                    $noExiste = $entityManager->getRepository('BackendBundle:Noticia')->findSlug($slug);
                    $i++;
                }else{
                    $noExiste = false;
                }
            }while($noExiste);
            $entity->setSlug($slug);
        }else if ($entity instanceof NoticiaTranslation) {
            if($entity->getField() == 'titulo'){
                $noExiste = 0;
                $slug = "";
                $i = 0;
                do{
                    $slug = Util::getSlug($entity->getContent(), '-', $i);
                    $noExiste = $entityManager->getRepository('BackendBundle:Noticia')->findSlugByIdioma($slug, $entity->getLocale());
                    $i++;
                }while($noExiste);
                $block = $entity->getObject();
                $slugTranslation = $entityManager->getRepository('BackendBundle:Noticia')->findTranlateSlugByObject($block->getId());

                //Si existe y es distinto actualizamos, sino pues añadimos (hacemos esta segunda comprobación en el if por que sino entra en un bucle infinito al guardar el mismo tipo de entidad)
                if($slugTranslation && $slugTranslation->getContent() != $slug){
                    $slugTranslation->setContent($slug);

                    $entityManager->flush();
                }elseif(!$slugTranslation){
                    $slugTranslation = new NoticiaTranslation($entity->getLocale(), 'slug', $slug);
                    $block->addTranslation($slugTranslation);

                    $entityManager->flush();
                }
            }
        }
    }
}