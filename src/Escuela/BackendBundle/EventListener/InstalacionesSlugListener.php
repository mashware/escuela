<?php
namespace Escuela\BackendBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Escuela\BackendBundle\Entity\Instalaciones;
use Escuela\BackendBundle\Utiles\Util;

/**
 * Class InstalacionesSlugListener
 * @package Escuela\BackendBundle\Event\EventListener
 */
class InstalacionesSlugListener {
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Instalaciones) {
            $noExiste = 0;
            $carpeta = "";
            $i = 0;

            do{
                $carpeta = Util::getSlug($entity->getTitulo(), '-', $i);
                $noExiste = $entityManager->getRepository('BackendBundle:Instalaciones')->findCarpeta($carpeta);
                $i++;
            }while($noExiste);
            $entity->setCarpeta($carpeta);
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {

    }
}