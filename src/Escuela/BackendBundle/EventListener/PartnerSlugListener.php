<?php
namespace Escuela\BackendBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Escuela\BackendBundle\Entity\Partner;
use Escuela\BackendBundle\Entity\PartnerTranslation;
use Escuela\BackendBundle\Utiles\Util;

/**
 * Class NoticiaSlugListener
 * @package Escuela\BackendBundle\Event\EventListener
 */
class PartnerSlugListener {
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Partner) {
            $noExiste = 0;
            $slug = "";
            $i = 0;

            do{
                $slug = Util::getSlug($entity->getNombre(), '-', $i);
                $noExiste = $entityManager->getRepository('BackendBundle:Partner')->findSlug($slug);
                $i++;
            }while($noExiste);
            $entity->setSlug($slug);
        }else if ($entity instanceof PartnerTranslation) {
            if($entity->getField() == 'nombre'){
                $noExiste = 0;
                $slug = "";
                $i = 0;
                do{
                    $slug = Util::getSlug($entity->getContent(), '-', $i);
                    $noExiste = $entityManager->getRepository('BackendBundle:Partner')->findSlugByIdioma($slug, $entity->getLocale());
                    $i++;
                }while($noExiste);
                $block = $entity->getObject();
                $slugTranslation = new PartnerTranslation($entity->getLocale(), 'slug', $slug);
                $block->addTranslation($slugTranslation);
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();
        if ($entity instanceof Partner) {
            $noExiste = 0;
            $slug = "";
            $i = 0;

            do{
                $slug = Util::getSlug($entity->getNombre(), '-', $i);
                if ($slug != $entity->getSlug()){
                    $noExiste = $entityManager->getRepository('BackendBundle:Partner')->findSlug($slug);
                    $i++;
                }else{
                    $noExiste = false;
                }
            }while($noExiste);
            $entity->setSlug($slug);
        }else if ($entity instanceof PartnerTranslation) {
            if($entity->getField() == 'nombre'){
                $noExiste = 0;
                $slug = "";
                $i = 0;
                do{
                    $slug = Util::getSlug($entity->getContent(), '-', $i);
                    $noExiste = $entityManager->getRepository('BackendBundle:Partner')->findSlugByIdioma($slug, $entity->getLocale());
                    $i++;
                }while($noExiste);
                $block = $entity->getObject();
                $slugTranslation = $entityManager->getRepository('BackendBundle:Partner')->findTranlateSlugByObject($block->getId());

                //Si existe y es distinto actualizamos, sino pues añadimos (hacemos esta segunda comprobación en el if por que sino entra en un bucle infinito al guardar el mismo tipo de entidad)
                if($slugTranslation && $slugTranslation->getContent() != $slug){
                    $slugTranslation->setContent($slug);

                    $entityManager->flush();
                }elseif(!$slugTranslation){
                    $slugTranslation = new PartnerTranslation($entity->getLocale(), 'slug', $slug);
                    $block->addTranslation($slugTranslation);

                    $entityManager->flush();
                }
            }
        }
    }
}