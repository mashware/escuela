<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NoticiaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('introduccion')
            ->add('texto', 'ckeditor')
            ->add('escuela', 'checkbox', array(
                'label'     => 'Escuela',
                'required'  => false,
            ))
            ->add('circuito', 'checkbox', array(
                'label'     => 'Circuito',
                'required'  => false,
            ))
            ->add('fuente', 'url', array(
                'required'  => false,
            ))
            ->add('estado')
            ->add('titulo_en', 'translated_field', array(
                'field'          => 'titulo',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\NoticiaTranslation',
            ))
            ->add('introduccion_en', 'translated_field', array(
                'field'          => 'introduccion',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\NoticiaTranslation',

            ))
            ->add('texto_en', 'translated_field', array(
                'field'          => 'texto',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\NoticiaTranslation',
                'widget' => 'ckeditor'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\Noticia'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_noticia';
    }
}
