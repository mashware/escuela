<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PublicidadType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('texto', 'ckeditor')
            ->add('foto', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('fotoTitulo', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('position')
            ->add('url')
            ->add('state')
            ->add('texto_ingles', 'translated_field', array(
                'field'          => 'texto',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\publicidadTranslation',
                'widget' => 'ckeditor'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\Publicidad'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_publicidad';
    }
}
