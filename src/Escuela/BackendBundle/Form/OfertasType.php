<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OfertasType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('precio')
            ->add('texto', 'ckeditor')
            ->add('desde', 'date', array(
                'widget'  => 'single_text'
            ))
            ->add('hasta', 'date', array(
                'widget'  => 'single_text'
            ))
            ->add('dias')
            ->add('imagen', 'hidden')
            ->add('foto', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('titulo_ingles', 'translated_field', array(
                'field'          => 'titulo',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\OfertasTranslation',
            ))
            ->add('texto_ingles', 'translated_field', array(
                'field'          => 'texto',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\OfertasTranslation',
                'widget' => 'ckeditor'
            ))
            ->add('dias_ingles', 'translated_field', array(
                'field'          => 'dias',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\OfertasTranslation',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\Ofertas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_ofertas';
    }
}
