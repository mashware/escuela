<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TarifaPDFUploadType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pdf', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('pdf_ingles', 'file', array(
                'mapped' => false,
                'required' => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\Tarifa'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_tarifa_pdf';
    }
}
