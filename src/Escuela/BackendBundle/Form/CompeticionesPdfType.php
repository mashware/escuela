<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CompeticionesPdfType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idioma', 'choice', array(
                'choices' => array('es' => 'Español', 'en' => 'Ingles'),
                'preferred_choices' => array('es'),
                'mapped' => false,
                'required' => false,
                'empty_value' => false,
            ))
            ->add('circular', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('ranking', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('horarios', 'file', array(
                'mapped' => false,
                'required' => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\Competiciones'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_competicionespdf';
    }
}
