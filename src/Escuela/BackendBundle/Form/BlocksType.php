<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlocksType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('text', 'ckeditor', array(
                'config_name' => 'my_config',))
            ->add('foto', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('url', 'text', array('required' => false))
            ->add('position')
            ->add('state', 'checkbox', array('required' => false))
            ->add('fullText', 'ckeditor', array('required' => false))
            ->add('blockSections', 'entity', array(
                'empty_value' => false,
                'class' => 'BackendBundle:BlockSections',
            ))
            ->add('title_en', 'translated_field', array(
                'field'          => 'title',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\BlocksTranslation',
            ))
            ->add('text_en', 'translated_field', array(
                'field'          => 'text',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\BlocksTranslation',
                'widget' => 'ckeditor'
            ))
            ->add('fullText_en', 'translated_field', array(
                'field'          => 'fullText',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\BlocksTranslation',
                'widget' => 'ckeditor'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\Blocks'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_blocks';
    }
}
