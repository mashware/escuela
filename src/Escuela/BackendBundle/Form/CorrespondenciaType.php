<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CorrespondenciaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('direccion')
            ->add('provincia')
            ->add('telefono')
            ->add('email', 'email')
            ->add('precioVisitantes')
            ->add('precioClub')
            ->add('imagen', 'hidden')
            ->add('foto', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('titulo_ingles', 'translated_field', array(
                'field'          => 'titulo',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\CorrespondenciaTranslation',
                'texto_label' => "Titulo"
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\Correspondencia'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_correspondencia';
    }
}
