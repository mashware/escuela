<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PartnerType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('provincia')
            ->add('descripcion', 'textarea')
            ->add('texto', 'ckeditor')
            ->add('oro', 'checkbox', array('required' => false))
            ->add('grande', 'checkbox', array('required' => false))
            ->add('telefono', 'text')
            ->add('contacto')
            ->add('estado', 'checkbox', array('required' => false))
            ->add('fotos', 'file', array(
                'mapped' => false,
                'required' => false,
                'multiple' => true
            ))
            ->add('principal', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('orden', 'integer', array(
                'attr' => array('min' => 0),
                'required' => false
            ))
            ->add('destacado', 'checkbox', array('required' => false))
            ->add('ordenDestacado', 'integer', array(
                'attr' => array('min' => 0),
                'required' => false
            ))
            ->add('nombre_ingles', 'translated_field', array(
                'field'          => 'nombre',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\PartnerTranslation',
            ))
            ->add('descripcion_ingles', 'translated_field', array(
                'field'          => 'descripcion',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\PartnerTranslation',
            ))
            ->add('texto_ingles', 'translated_field', array(
                'field'          => 'texto',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\PartnerTranslation',
                'widget' => 'ckeditor'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\Partner'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_partner';
    }
}
