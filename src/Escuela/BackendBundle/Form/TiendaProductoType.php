<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TiendaProductoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('descripcion')
            ->add('precio')
            ->add('detalles', 'ckeditor')
            ->add('estado', 'checkbox', array(
                'required' => false
            ))
            ->add('saldo', 'checkbox', array(
                'required' => false
            ))
            ->add('fotos', 'file', array(
                'mapped' => false,
                'required' => false,
                'multiple' => true
            ))
            ->add('principal', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('nombre_ingles', 'translated_field', array(
                'field'          => 'nombre',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\TiendaProductoTranslation',
            ))
            ->add('descripcion_ingles', 'translated_field', array(
                'field'          => 'descripcion',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\TiendaProductoTranslation',
                'widget' => 'textarea'
            ))
            ->add('detalles_ingles', 'translated_field', array(
                'field'          => 'detalles',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\TiendaProductoTranslation',
                'widget' => 'ckeditor'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\TiendaProducto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_tiendaproducto';
    }
}
