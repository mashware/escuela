<?php
/**
 * Created by PhpStorm.
 * User: Alberto
 * Date: 4/05/14
 * Time: 20:15
 */

namespace Escuela\BackendBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Escuela\BackendBundle\Entity\MediaCategory;

class MediaCategoryHiddenTransformer implements DataTransformerInterface {
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }
    /**
     * Transforms an object (mediaCategory) to a string (number).
     *
     * @param  MediaCategory|null $slider
     * @return string
     */
    public function transform($slider)
    {
        if (null === $slider) {
            return "";
        }

        return $slider->getId();
    }
    /**
     * Transforms a string (number) to an object (slider).
     *
     * @param  string $number
     *
     * @return MediaCategory|null
     *
     * @throws TransformationFailedException if object (mediaCategory) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }

        $mediaCategory = $this->om
            ->getRepository('BackendBundle:MediaCategory')
            ->find($number)
        ;

        if (null === $mediaCategory) {
            throw new TransformationFailedException(sprintf(
                'An mediaCategory with number "%s" does not exist!',
                $number
            ));
        }

        return $mediaCategory;
    }
} 