<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HoyosType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('descripcion', 'ckeditor')
            ->add('video')
            ->add('fotos', 'file', array(
                'mapped' => false,
                'required' => false,
                'attr' => array(
                    'multiple' => 'multiple'
                )
            ))
            ->add('nombre_ingles', 'translated_field', array(
                'field'          => 'nombre',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\HoyosTranslation',
            ))
            ->add('descripcion_ingles', 'translated_field', array(
                'field'          => 'descripcion',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\HoyosTranslation',
                'widget' => 'ckeditor'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\Hoyos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_hoyos';
    }
}
