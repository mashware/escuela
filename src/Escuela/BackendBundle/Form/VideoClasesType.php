<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VideoClasesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('clave')
            ->add('imagen', 'file', array(
                'required' => false,
                'mapped' => false
            ))
            ->add('titulo_en', 'translated_field', array(
                'field'          => 'titulo',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\VideoClasesTranslation',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\VideoClases'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_videoclases';
    }
}
