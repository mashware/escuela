<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TarifaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('descripcion', 'ckeditor')
            ->add('contenido', 'ckeditor')
            ->add('foto', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('titulo_ingles', 'translated_field', array(
                'field'          => 'titulo',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\TarifaTranslation',
            ))
            ->add('descripcion_ingles', 'translated_field', array(
                'field'          => 'descripcion',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\TarifaTranslation',
                'widget' => 'ckeditor'
            ))
            ->add('contenido_ingles', 'translated_field', array(
                'field'          => 'contenido',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\TarifaTranslation',
                'widget' => 'ckeditor'
            ))
            ->add('foto_ingles', 'file', array(
                'mapped' => false,
                'required' => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\Tarifa'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_tarifa';
    }
}
