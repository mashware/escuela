<?php

namespace Escuela\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CompeticionesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('lugar')
            ->add('fecha', 'datetime', array(
                'format' => 'dd/MM/yyyy hh:mm',
                'widget'  => 'single_text'
            ))
            ->add('precio')
            ->add('categoria')
            ->add('foto', 'file', array(
                'mapped' => false,
                'required' => false
            ))
            ->add('nombre_ingles', 'translated_field', array(
                'field'          => 'nombre',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\CompeticionesTranslation',
                'texto_label' => "Nombre"
            ))
            ->add('lugar_ingles', 'translated_field', array(
                'field'          => 'lugar',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\CompeticionesTranslation',
                'texto_label' => "Lugar"
            ))
            ->add('categoria_ingles', 'translated_field', array(
                'field'          => 'categoria',
                'property_path'  => 'translations',
                'locales' => array('en'),
                'personal_translation' => 'Escuela\BackendBundle\Entity\CompeticionesTranslation',
                'texto_label' => "Categoria"
            ));

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Escuela\BackendBundle\Entity\Competiciones'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'escuela_backendbundle_competiciones';
    }
}
