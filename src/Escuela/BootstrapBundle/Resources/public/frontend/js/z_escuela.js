$(document).ready(function() {

    /* Mensaje Cookies */
    if($.cookie('msg_cookie_aceptado') != "true"){
        $('.info_cookies').show();
    }
    $('a').click(function(e){
        $.cookie('msg_cookie_aceptado', 'true', { path: '/' });
        $('.info_cookies').hide();
    });
    $('#aceptar_cookies').click(function(e){
        e.preventDefault();
        $.cookie('msg_cookie_aceptado', 'true', { path: '/' });
        $('.info_cookies').hide();
    });
    /******************/
    $('.bloque-mas').click(function(e){
        e.preventDefault();
        var id = $(this).attr('href');
        $(this).closest('.bloques').find('.activo').removeClass('activo');
        $(this).find('h1').addClass('activo');
        $('#contenedor-mas-bloques').find('>div:visible').fadeToggle(150, function(){
            var piramide = $('#piramide');
            piramide.removeClass();
            piramide.addClass(id.substr(1)+'-piramide');
            $(id).fadeToggle(150);
        });
    });

    $(".sidebar .treeview").tree();

    $('a.nivo').nivoLightbox({
        effect: 'fade',                             // The effect to use when showing the lightbox
        theme: 'default',                           // The lightbox theme to use
        keyboardNav: true,                          // Enable/Disable keyboard navigation (left/right/escape)
        clickOverlayToClose: true,                  // If false clicking the "close" button will be the only way to close the lightbox
        afterAddAjax: function(){
            $('#slider').nivoSlider();
        },
        errorMessage: 'The requested content cannot be loaded. Please try again later.' // Error message when content can't be loaded
    });
    $('a.lightbox').nivoLightbox({
        effect: 'fade',                             // The effect to use when showing the lightbox
        theme: 'default',                           // The lightbox theme to use
        keyboardNav: true,                          // Enable/Disable keyboard navigation (left/right/escape)
        clickOverlayToClose: true                  // If false clicking the "close" button will be the only way to close the lightbox
    });
    $('a.nivo-partner').nivoLightbox({
        effect: 'fade',                             // The effect to use when showing the lightbox
        theme: 'default',                           // The lightbox theme to use
        keyboardNav: true,                          // Enable/Disable keyboard navigation (left/right/escape)
        clickOverlayToClose: true,                  // If false clicking the "close" button will be the only way to close the lightbox
        afterAddAjax: function(){
            var primero = $('#slider img').index($('#primero'));
            $('#slider').nivoSlider({
                startSlide: primero + 1
            });
        },
        errorMessage: 'The requested content cannot be loaded. Please try again later.' // Error message when content can't be loaded
    });
});