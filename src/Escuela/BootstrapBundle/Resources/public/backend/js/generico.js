/*
*
* Mio de la aplicación
*
 */
$(document).ready(function(){
    $('.img-principal').click(function(e){
        e.preventDefault();
        var imagen = $(this).closest('.imagenes-list');
        $.get($(this).attr('href'), function(data){
            if(data){
                $('body').find('.principal').remove();
                imagen.append('<div class="principal">Principal</div>');
            }else{
                alert("Ha ocurrido un error");
            }
        });
    });
    $('.img-delete').click(function(e){
        e.preventDefault();
        var contenedor = $(this).closest('.imagenes-list');
        $.get($(this).attr('href'), function(data){
            if(data){
                contenedor.remove();
            }else{
                alert("Ha ocurrido un error");
            }
        });
    });
    $('.encoger').click(function(e){
        var container = $(this).closest('fieldset').find('.contenido-fieldset');
        if(container.is(':visible')){
            $(this).find('.estado').html('+');
        }else{
            $(this).find('.estado').html('-');
        }
        container.slideToggle();

    });
    //Money Euro
    //$("[data-mask]").inputmask();

});
